package controller

import (
	myConfig "common/config"
	myredis "common/redis"
	"errors"
	"fmt"
	"github.com/valyala/fasthttp"
	"member/contrib/helper"
	"member/model"
)

type ProxyController struct{}

type ProxyAccuInfo struct {
	TotalCommi float64 `json:"TotalCommi" db:"TotalCommi" cbor:"TotalCommi"` //总佣金
	TeamNum    float64 `json:"team_num" db:"team_num" cbor:"team_num"`       //邀请人数
	DepositNum int     `json:"DepositNum" db:"DepositNum" cbor:"DepositNum"` //充值人数
	ValidNum   float64 `json:"ValidNum" db:"ValidNum" cbor:"ValidNum"`       //有效人数
	Deposit    float64 `json:"Deposit" db:"Deposit" cbor:"Deposit"`          //总充值金额
	Running    float64 `json:"Running" db:"Running" cbor:"Running"`          //总流水
}

func GetAccuInfoFromSql(valUid string) (ProxyAccuInfo, bool) {
	var tAccuInfo ProxyAccuInfo
	sqlSelect := fmt.Sprintf("select proxy_invite_bonus,team_num,deposit_num,valid_num,deposit,running from tbl_member_accu where uid=%s and level=1", valUid)
	rows, err := model.GetDBInstance().Query(sqlSelect)
	if err != nil {
		return tAccuInfo, false
	}

	for rows.Next() {
		rows.Scan(&tAccuInfo.TotalCommi, &tAccuInfo.TeamNum,
			&tAccuInfo.DepositNum, &tAccuInfo.ValidNum, &tAccuInfo.Deposit, &tAccuInfo.Running)
	}

	return tAccuInfo, true
}

func (that ProxyController) QueryAccuInfo(ctx *fasthttp.RequestCtx) {
	valUid := model.GetUidFromToken(ctx)
	if valUid == "" {
		helper.Print(ctx, false, errors.New(helper.AccessTokenExpires))
		return
	}

	tAccuInfo, bReturn := GetAccuInfoFromSql(valUid)
	helper.Print(ctx, bReturn, tAccuInfo)
}

/*
查询代理周佣金
*/
func (that ProxyController) QueryWeekDepositReturn(ctx *fasthttp.RequestCtx) {
	valUid := model.GetUidFromToken(ctx)
	if valUid == "" {
		helper.Print(ctx, false, errors.New(helper.AccessTokenExpires))
		return
	}

	fWeekDepositReturn := GetProxyWeekDepositFromDB(valUid)
	helper.Print(ctx, true, fWeekDepositReturn)
}

func GetProxyWeekDepositFromDB(valUid string) float64 {
	var fWeekDepositReturn float64

	sqlSelect := fmt.Sprintf("select week_proxy_commi_return from tbl_member_return where uid=%s", valUid)
	rows, err := model.GetDBInstance().Query(sqlSelect)
	if err != nil {
		return fWeekDepositReturn
	}

	for rows.Next() {
		rows.Scan(&fWeekDepositReturn)
	}

	return fWeekDepositReturn
}

/*
领取代理周佣金
*/

func (that ProxyController) AchieveWeekDepositReturn(ctx *fasthttp.RequestCtx) {
	valUid := model.GetUidFromToken(ctx)
	if valUid == "" {
		helper.Print(ctx, false, errors.New(helper.AccessTokenExpires))
		return
	}

	fWeekDepositReturn := GetProxyWeekDepositFromDB(valUid)
	myredis.AddUserFieldValueByFloat64(valUid, myConfig.G_tbl_member_balance+"brl", fWeekDepositReturn)
	helper.Print(ctx, true, fWeekDepositReturn)
}

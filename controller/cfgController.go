package controller

import (
	myConfig "common/config"
	"github.com/valyala/fasthttp"
	"member/contrib/helper"
)

type ConfigController struct{}

func (that ConfigController) WeekDepositReturn(ctx *fasthttp.RequestCtx) {

	var cfg myConfig.CsDepositReturnCfg

	data, err := cfg.GetCfg()
	if err != nil {
		helper.Print(ctx, false, err)
		return
	}
	helper.Print(ctx, true, data)
}

func (that ConfigController) WeekLostReturn(ctx *fasthttp.RequestCtx) {

	var cfg myConfig.CsLostReturnCfg

	data, err := cfg.GetCfg()
	if err != nil {
		helper.Print(ctx, false, err)
		return
	}
	helper.Print(ctx, true, data)
}
func (that ConfigController) WeekRunningReturn(ctx *fasthttp.RequestCtx) {

	var cfg myConfig.CsRunningReturnCfg

	data, err := cfg.GetCfg()
	if err != nil {
		helper.Print(ctx, false, err)
		return
	}
	helper.Print(ctx, true, data)
}
func (that ConfigController) WeekProxyCommiReturn(ctx *fasthttp.RequestCtx) {

	var cfg myConfig.CsProxyCommiReturnCfg

	data, err := cfg.GetCfg()
	if err != nil {
		helper.Print(ctx, false, err)
		return
	}
	helper.Print(ctx, true, data)
}

// 累计充值奖励配置
func (that ConfigController) AccuDepositBonus(ctx *fasthttp.RequestCtx) {

	var cfg myConfig.CsAccuDepositBonusCfg

	data, err := cfg.GetCfg()
	if err != nil {
		helper.Print(ctx, false, err)
		return
	}
	helper.Print(ctx, true, data)
}

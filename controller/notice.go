package controller

import (
	"member/contrib/helper"
	"member/model"

	"github.com/valyala/fasthttp"
)

type NoticeController struct{}

func (that *NoticeController) List(ctx *fasthttp.RequestCtx) {

	data, err := model.NoticeList()
	if err != nil {
		helper.Print(ctx, false, err)
		return
	}
	ctx.SetBody(data)
}

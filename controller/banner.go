package controller

import (
	"member/contrib/helper"
	"member/model"
	"strconv"

	"github.com/valyala/fasthttp"
)

type BannerController struct{}

func (that *BannerController) List(ctx *fasthttp.RequestCtx) {

	d := string(ctx.Request.Header.Peek("d"))
	i, err := strconv.Atoi(d)
	if err != nil {
		helper.Print(ctx, false, helper.DeviceTypeErr)
		return
	}
	if _, ok := model.Devices[i]; !ok {
		helper.Print(ctx, false, helper.DeviceTypeErr)
		return
	}
	data, err := model.BannerList(d)
	if err != nil {
		helper.Print(ctx, false, err)
		return
	}
	ctx.SetBody(data)
}

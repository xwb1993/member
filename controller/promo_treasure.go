package controller

import (
	myUserHelp "common/userHelp"
	"github.com/valyala/fasthttp"
	"member/contrib/helper"
	"member/model"
)

type PromoTreasureController struct{}

// 宝箱活动-配置
func (that PromoTreasureController) Config(ctx *fasthttp.RequestCtx) {

	s, err := model.PromoTreasureConfigList()
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 宝箱活动-申请记录
func (that PromoTreasureController) Record(ctx *fasthttp.RequestCtx) {

	uid := model.GetUidFromToken(ctx)
	if uid == "" {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		helper.Print(ctx, false, helper.UserNotExist)
		return
	}

	// 代理
	if mb.Tester == 2 {
		helper.Print(ctx, false, helper.PromoApplyFailed)
		return
	}

	s, err := model.PromoTreasureMemberRecord(mb.Uid)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 宝箱活动-申请
func (that PromoTreasureController) Apply(ctx *fasthttp.RequestCtx) {

	inviteNum := ctx.QueryArgs().GetUintOrZero("invite_num")
	if inviteNum <= 0 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	uid := model.GetUidFromToken(ctx)
	if uid == "" {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		helper.Print(ctx, false, helper.UserNotExist)
		return
	}

	if mb.CanBonus == 2 {
		helper.Print(ctx, false, helper.NoAwardCollect)
		return
	}

	// 代理
	if mb.Tester == 2 {
		helper.Print(ctx, false, helper.PromoApplyFailed)
		return
	}

	err := model.PromoTreasureApply(mb, inviteNum)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

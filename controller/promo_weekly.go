package controller

import (
	"github.com/valyala/fasthttp"
	"member/contrib/helper"
	"member/model"
)

type PromoWeeklyController struct{}

func (that PromoWeeklyController) Config(ctx *fasthttp.RequestCtx) {

	uid := model.GetUidFromToken(ctx)
	if uid == "" {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	s, err := model.PromoWeeklyConfigList(uid)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

package controller

import (
	myConfig "common/config"
	myredis "common/redis"
	myUserHelp "common/userHelp"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
	"member/contrib/helper"
	"member/model"
	"time"
)

//玩家周返回相关

type WeekReturn struct {
	DeposiReturn float64 `json:"DeposiReturn" db:"DeposiReturn" cbor:"DeposiReturn"` //总佣金
	BetReturn    float64 `json:"BetReturn" db:"BetReturn" cbor:"BetReturn"`          //邀请人数
	LostReturn   float64 `json:"LostReturn" db:"LostReturn" cbor:"LostReturn"`       //充值人数
}

func GetWeekReturnFromDB(valUid string) (WeekReturn, bool) {
	var tWeekReturn WeekReturn
	sqlSelect := fmt.Sprintf("select week_deposit_return,week_bet_return,week_lost_return from tbl_member_return where uid=%s", valUid)
	rows, err := model.GetDBInstance().Query(sqlSelect)
	if err != nil {
		return tWeekReturn, false
	}

	for rows.Next() {
		rows.Scan(&tWeekReturn.DeposiReturn, &tWeekReturn.BetReturn, &tWeekReturn.LostReturn)
	}

	return tWeekReturn, true
}

func (that MemberController) QueryWeekReturnInfo(ctx *fasthttp.RequestCtx) {
	valUid := model.GetUidFromToken(ctx)
	if valUid == "" {
		helper.Print(ctx, false, errors.New(helper.AccessTokenExpires))
		return
	}

	tWeekReturn, bReturn := GetWeekReturnFromDB(valUid)
	helper.Print(ctx, bReturn, tWeekReturn)
}

/*
领取玩家周充值返还
*/
func (that MemberController) AchieveDepoisutReturn(ctx *fasthttp.RequestCtx) {
	valUid := model.GetUidFromToken(ctx)
	if valUid == "" {
		helper.Print(ctx, false, errors.New(helper.AccessTokenExpires))
		return
	}

	tWeekReturn, _ := GetWeekReturnFromDB(valUid)
	myredis.AddUserFieldValueByFloat64(valUid, myConfig.G_tbl_member_balance+"brl", tWeekReturn.DeposiReturn)
	sqlSelect := fmt.Sprintf("update tbl_member_return set week_deposit_return=0 where uid=%s", valUid)
	model.GetDBInstance().Exec(sqlSelect)
	helper.Print(ctx, true, tWeekReturn.DeposiReturn)

	amount := decimal.NewFromFloat(tWeekReturn.DeposiReturn)
	balance := myUserHelp.GetBalanceByUid(valUid, 2)
	balanceAfter := balance.Add(amount)
	id := helper.GenId()
	mb, _ := myUserHelp.GetMemberBaseInfoByUid(valUid)
	trans := model.MemberTransaction{
		AfterAmount:  balanceAfter.String(),
		Amount:       amount.String(),
		BeforeAmount: balance.String(),
		BillNo:       id,
		CreatedAt:    time.Now().UnixMilli(),
		ID:           id,
		CashType:     helper.TransactionWeekCommiReturn,
		UID:          valUid,
		Username:     mb.Username,
		Remark:       "",
	}

	query, _, _ := g.Dialect("mysql").Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	myredis.AddLogToRedis(query)
}

/*
玩家周投注返利
*/
func (that MemberController) AchieveBetReturn(ctx *fasthttp.RequestCtx) {
	valUid := model.GetUidFromToken(ctx)
	if valUid == "" {
		helper.Print(ctx, false, errors.New(helper.AccessTokenExpires))
		return
	}

	tWeekReturn, _ := GetWeekReturnFromDB(valUid)
	myredis.AddUserFieldValueByFloat64(valUid, myConfig.G_tbl_member_balance+"brl", tWeekReturn.BetReturn)
	sqlSelect := fmt.Sprintf("update tbl_member_return set week_bet_return=0 where uid=%s", valUid)
	model.GetDBInstance().Exec(sqlSelect)
	helper.Print(ctx, true, tWeekReturn.BetReturn)

	amount := decimal.NewFromFloat(tWeekReturn.BetReturn)
	balance := myUserHelp.GetBalanceByUid(valUid, 2)
	balanceAfter := balance.Add(amount)
	id := helper.GenId()
	mb, _ := myUserHelp.GetMemberBaseInfoByUid(valUid)
	trans := model.MemberTransaction{
		AfterAmount:  balanceAfter.String(),
		Amount:       amount.String(),
		BeforeAmount: balance.String(),
		BillNo:       id,
		CreatedAt:    time.Now().UnixMilli(),
		ID:           id,
		CashType:     helper.TransactionWeekBetReturn,
		UID:          valUid,
		Username:     mb.Username,
		Remark:       "",
	}

	query, _, _ := g.Dialect("mysql").Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	myredis.AddLogToRedis(query)
}

/*
玩家周亏损返利
*/
func (that MemberController) AchieveLostReturn(ctx *fasthttp.RequestCtx) {
	valUid := model.GetUidFromToken(ctx)
	if valUid == "" {
		helper.Print(ctx, false, errors.New(helper.AccessTokenExpires))
		return
	}

	tWeekReturn, _ := GetWeekReturnFromDB(valUid)
	myredis.AddUserFieldValueByFloat64(valUid, myConfig.G_tbl_member_balance+"brl", tWeekReturn.LostReturn)
	sqlSelect := fmt.Sprintf("update tbl_member_return set week_lost_return=0 where uid=%s", valUid)
	model.GetDBInstance().Exec(sqlSelect)
	helper.Print(ctx, true, tWeekReturn.LostReturn)

	amount := decimal.NewFromFloat(tWeekReturn.LostReturn)
	balance := myUserHelp.GetBalanceByUid(valUid, 2)
	balanceAfter := balance.Add(amount)
	id := helper.GenId()
	mb, _ := myUserHelp.GetMemberBaseInfoByUid(valUid)
	trans := model.MemberTransaction{
		AfterAmount:  balanceAfter.String(),
		Amount:       amount.String(),
		BeforeAmount: balance.String(),
		BillNo:       id,
		CreatedAt:    time.Now().UnixMilli(),
		ID:           id,
		CashType:     helper.TransactionWeekLostReturn,
		UID:          valUid,
		Username:     mb.Username,
		Remark:       "",
	}

	query, _, _ := g.Dialect("mysql").Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	myredis.AddLogToRedis(query)
}

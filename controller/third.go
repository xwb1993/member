package controller

import (
	"encoding/json"
	"fmt"
	"github.com/valyala/fasthttp"
	"member/contrib/helper"
	"member/contrib/validator"
	"member/model"
)

type ThirdController struct{}

var onlinePay = map[string]bool{
	"666777888": true, //	EPAY
	"666777999": true, //	EPAYU
}

type depositParam struct {
	Fid    string `json:"fid" cbor:"fid"`
	Amount string `json:"amount" cbor:"amount"`
	Flag   string `json:"flag" cbor:"flag"` //1 参与 2 不参与
}

type withdrawParam struct {
	Amount      string `json:"amount" cbor:"amount"`             //金额
	BankId      string `json:"bank_id" cbor:"bank_id"`           //银行id
	PayPassword string `json:"pay_password" cbor:"pay_password"` //支付密码
	Fid         string `json:"fid" cbor:"fid"`
}

func (that ThirdController) Deposit(ctx *fasthttp.RequestCtx) {

	prefix := string(ctx.Request.Header.Peek("X-Prefix"))

	param := depositParam{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	if !helper.CtypeDigit(param.Amount) {
		helper.Print(ctx, false, helper.AmountErr)
		return
	}

	// 在线支付走if里面的代码
	//if _, ok := onlinePay[param.Fid]; ok {
	//	res, err := model.PayOnline(ctx, prefix, param.Fid, param.Amount, param.Flag)
	//	if err != nil {
	//		helper.Print(ctx, false, err.Error())
	//		return
	//	}
	//	helper.Print(ctx, true, res)
	//	return
	//}
	res, err := model.Recharge(ctx, prefix, param.Fid, param.Amount, param.Flag)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, res)
	//return
	//helper.Print(ctx, false, helper.PayServerErr)
}

func (that ThirdController) DepositChannel(ctx *fasthttp.RequestCtx) {

	data, err := model.ChannelList(ctx, 1)
	if err != nil {
		helper.Print(ctx, false, err)
		return
	}
	helper.Print(ctx, true, data)
}

func (that ThirdController) WithdrawConfig(ctx *fasthttp.RequestCtx) {

	data, err := model.WithdrawConfig(ctx)
	if err != nil {
		helper.Print(ctx, false, err)
		return
	}
	helper.Print(ctx, true, data)
}

func (that ThirdController) CallbackEpay(ctx *fasthttp.RequestCtx) {

	model.DepositCallBack(ctx)
}

//func (that ThirdController) CallbackEpayUsdt(ctx *fasthttp.RequestCtx) {
//
//	model.DepositCallBack(ctx, "666777999")
//}

func (that ThirdController) Withdraw(ctx *fasthttp.RequestCtx) {

	prefix := string(ctx.Request.Header.Peek("X-Prefix"))

	param := withdrawParam{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	if !validator.CheckMoney(param.Amount) {
		helper.Print(ctx, false, helper.AmountErr)
		return
	}
	if param.BankId == "" {
		helper.Print(ctx, false, helper.BankcardIDErr)
		return
	}

	res, err := model.WithdrawApply(ctx, prefix, param.Amount, param.BankId, param.PayPassword, param.Fid)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, res)
	return
}

func (that ThirdController) CallbackEpayW(ctx *fasthttp.RequestCtx) {
	fmt.Println("提现回调")
	model.WithdrawalCallBack(ctx)
}

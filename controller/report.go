package controller

import (
	"encoding/json"
	"fmt"
	"github.com/valyala/fasthttp"
	"member/contrib/helper"
	"member/model"
)

type ReportController struct{}

type listReportParam struct {
	Lvl       string `json:"lvl"`
	StartTime string `json:"start_time"`
	EndTime   string `json:"end_time"`
	Page      uint   `json:"page"`
	PageSize  uint   `json:"page_size"`
}

func (that *ReportController) Detail(ctx *fasthttp.RequestCtx) {

	uid := model.GetUidFromToken(ctx)
	if uid == "" {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	res, err := model.ReportDetail(uid)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, res)
}

func (that *ReportController) List(ctx *fasthttp.RequestCtx) {

	param := listReportParam{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParaJsonUnmarshalErr)
		return
	}

	if param.Page < 1 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	uid := model.GetUidFromToken(ctx)
	if uid == "" {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	res, err := model.ReportRecord(uid, param.StartTime, param.EndTime, param.Lvl, uint(param.Page), uint(param.PageSize))
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, res)
}

package controller

import (
	myUserHelp "common/userHelp"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/valyala/fasthttp"
	"member/contrib/helper"
	"member/model"
)

type TurnTableController struct{}

func (that *TurnTableController) Info(ctx *fasthttp.RequestCtx) {

	uid := model.GetUidFromToken(ctx)
	if uid == "" {
		helper.Print(ctx, false, errors.New(helper.AccessTokenExpires))
		return
	}

	m, ret := myUserHelp.GetPddTurntableInfo(uid)
	if !ret {
		helper.Print(ctx, false, errors.New(helper.UserNotExist))
		return
	}

	helper.Print(ctx, true, m)
}

func (that *TurnTableController) Click(ctx *fasthttp.RequestCtx) {

	uid := string(ctx.QueryArgs().Peek("uid"))
	if !helper.CtypeDigit(uid) {
		helper.Print(ctx, false, helper.UIDErr)
		return
	}
	reward, err := model.TurnTableClick(uid)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, reward)
}

func (that TurnTableController) Apply(ctx *fasthttp.RequestCtx) {
	param := model.ApplyParam{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	if param.Amount < 100 {
		helper.Print(ctx, false, helper.RewardAmountUnreached)
		return
	}
	err = model.Apply(param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, helper.Success)
}

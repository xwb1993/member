package controller

import (
	"common/config"
	myUserHelp "common/userHelp"
	"github.com/valyala/fasthttp"
	"member/contrib/helper"
	"member/model"
)

type PromoDepositController struct{}

func (that PromoDepositController) Config(ctx *fasthttp.RequestCtx) {

	uid := model.GetUidFromToken(ctx)
	if uid == "" {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		helper.Print(ctx, false, helper.UserNotExist)
		return

	}

	ty := 2
	if mb.DepositAmount == 0 {
		ty = 1
	}
	s := config.PromoDepositConfigList(ty)

	helper.Print(ctx, true, s)
}

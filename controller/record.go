package controller

import (
	myUserHelp "common/userHelp"
	"encoding/json"
	"fmt"
	"github.com/valyala/fasthttp"
	"member/contrib/helper"
	"member/model"
)

type RecordController struct{}

func (that *RecordController) Trade(ctx *fasthttp.RequestCtx) {

	flag := ctx.QueryArgs().GetUintOrZero("flag") //账变类型 271 存款 272 取款
	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")

	if page < 1 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	uid := model.GetUidFromToken(ctx)
	if uid == "" {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	user, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		helper.Print(ctx, false, helper.UserNotExist)
		return
	}

	data, err := model.RecordTrade(user.Uid, flag, uint(page), uint(pageSize))
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

func (that *RecordController) GameRecord(ctx *fasthttp.RequestCtx) {

	gameTy := ctx.QueryArgs().GetUintOrZero("ty") //1	真人 2	捕鱼 3	电子 4	体育 5	棋牌 6	电竞
	flag := ctx.QueryArgs().GetUintOrZero("flag") //1 今天 7 七天 60 六十天
	gt := ctx.QueryArgs().GetUintOrZero("gt")     //0 全部1为赢的
	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	if page < 1 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	uid := model.GetUidFromToken(ctx)
	if uid == "" {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	data, err := model.GameRecord(uid, gameTy, flag, gt, uint(page), uint(pageSize))
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, data)
}

func (that *RecordController) BonusRecord(ctx *fasthttp.RequestCtx) {

	ty := ctx.QueryArgs().GetUintOrZero("ty")     //0全部 6存款优惠 305邀请奖励  307宝箱奖励
	flag := ctx.QueryArgs().GetUintOrZero("flag") //1 今天 7 七天 60 六十天
	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")

	if ty > 0 {
		if _, ok := helper.CashTypes[ty]; !ok {
			helper.Print(ctx, false, helper.CashTypeErr)
			return
		}
	}
	if page < 1 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	uid := model.GetUidFromToken(ctx)
	if uid == "" {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	data, err := model.BonusRecord(uid, ty, flag, uint(page), uint(pageSize))
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, data)
}

type inviteRecordParam struct {
	Lvl       string `json:"lvl"`  //123一级二级三级
	Flag      string `json:"flag"` //0全部1有充值2没有充值
	StartTime string `json:"start_time"`
	EndTime   string `json:"end_time"`
	Page      uint   `json:"page"`
	PageSize  uint   `json:"page_size"`
}

func (that *RecordController) InviteRecord(ctx *fasthttp.RequestCtx) {

	param := inviteRecordParam{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if param.Page < 1 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	uid := model.GetUidFromToken(ctx)
	if uid == "" {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	user, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		helper.Print(ctx, false, helper.UserNotExist)
		return
	}

	res, err := model.InviteRecord(user.Uid, param.StartTime, param.EndTime, param.Flag, param.Lvl, uint(param.Page), uint(param.PageSize))
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, res)
}

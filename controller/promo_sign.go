package controller

import (
	myUserHelp "common/userHelp"
	"github.com/valyala/fasthttp"
	"member/contrib/helper"
	"member/model"
)

type PromoSignController struct{}

// 签到活动配置
func (that PromoSignController) Config(ctx *fasthttp.RequestCtx) {

	s, err := model.PromoSignConfigList()
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 签到活动-会员签到记录
func (that PromoSignController) Record(ctx *fasthttp.RequestCtx) {

	uid := model.GetUidFromToken(ctx)
	if uid == "" {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		helper.Print(ctx, false, helper.UserNotExist)
		return
	}

	s, err := model.PromoSignMemberRecord(mb)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 签到活动-会员签到奖励领取记录
func (that PromoSignController) RewardRecord(ctx *fasthttp.RequestCtx) {

	uid := model.GetUidFromToken(ctx)
	if uid == "" {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		helper.Print(ctx, false, helper.UsernameErr)
		return
	}

	s, err := model.PromoSignMemberRewardRecord(mb.Uid)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 签到活动-会员签到记录
func (that PromoSignController) Sign(ctx *fasthttp.RequestCtx) {

	weekDays := ctx.QueryArgs().GetUintOrZero("weekDays")
	monthDays := ctx.QueryArgs().GetUintOrZero("monthDays")
	if weekDays < 1 || weekDays > 7 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	if monthDays < 1 || monthDays > 30 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	uid := model.GetUidFromToken(ctx)
	if uid == "" {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		helper.Print(ctx, false, helper.UserNotExist)
		return
	}

	if mb.CanBonus == 2 {
		helper.Print(ctx, false, helper.NoAwardCollect)
		return
	}

	err := model.PromoSign(mb, weekDays, monthDays)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

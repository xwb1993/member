package main

import (
	"common/config"
	myredis "common/redis"
	"common/userHelp"
	"context"
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/meilisearch/meilisearch-go"
	"github.com/valyala/fasthttp"
	_ "go.uber.org/automaxprocs"
	"log"
	"member/contrib/apollo"
	"member/contrib/conn"
	"member/middleware"
	"member/model"
	"member/pkg"
	"member/router"
	"os"
	"path/filepath"
	"strings"
)

func main() {

	argc := len(os.Args)
	if argc != 4 {
		fmt.Printf("%s <etcds> <cfgPath> <web|fav>\r\n", os.Args[0])
		return
	}

	cfg := pkg.Conf{}
	endpoints := strings.Split(os.Args[1], ",")
	apollo.New(endpoints, pkg.ETCDName, pkg.ETCDPass)
	//err := apollo.ParseTomlStruct(os.Args[2], &cfg)
	_, err := toml.DecodeFile(os.Args[2], &cfg)
	apollo.Close()
	if err != nil {
		fmt.Printf("ParseTomlStruct error: %s", err.Error())
		return
	}

	mt := new(model.MetaTable)

	mt.Email = cfg.Email
	mt.AwsEmail = cfg.AwsEmail
	mt.IpDB = conn.InitIpDB(cfg.Ipdb)
	mt.MerchantDB = conn.InitDB(cfg.Db.Member, cfg.Db.MaxIdleConn, cfg.Db.MaxOpenConn)
	mt.MerchantBean = conn.BeanNew(cfg.Beanstalkd)
	mt.MerchantRedis = conn.InitRedis(cfg.Redis.Addr[0], cfg.Redis.Password, 0)
	//mt.MerchantRedis = conn.InitRedisSentinel(cfg.Redis.Addr, cfg.Redis.Password, cfg.Redis.Sentinel, 0)
	mt.Meili = meilisearch.NewClient(meilisearch.ClientConfig{
		Host:   cfg.Meilisearch.Host,
		APIKey: cfg.Meilisearch.Key,
	})
	mt.TgPay.AppKey = cfg.Tgpay.AppKey
	mt.WalletMode = cfg.WalletMode

	model.New("0.0.0.0")

	model.Constructor(mt, cfg.RPC)

	if os.Args[3] == "fav" {
		model.FavFlushAll()
		return
	}
	defer func() {
		model.Close()
		mt = nil
	}()

	mt.Program = filepath.Base(os.Args[0])
	b := router.BuildInfo{
		GitReversion:   pkg.GitReversion,
		BuildTime:      pkg.BuildTime,
		BuildGoVersion: pkg.BuildGoVersion,
	}
	mt.Contate = cfg.Contate
	mt.WebUrl = cfg.WebURL
	mt.Callback = cfg.Callback
	app := router.SetupRouter(b)

	//c := cron.New(cron.WithSeconds())
	//// 每隔5分钟执行一次，同步redis数据到mysql
	//_, err = c.AddFunc("0 0/5 * * * ?", model.UpdateQueue)
	//if err != nil {
	//	log.Fatalln("无法设置定时任务: ", err)
	//}
	//c.Start()

	config.InitCfg(mt.MerchantDB, mt.MerchantRedis)
	myredis.Init(context.Background(), mt.MerchantRedis, mt.MerchantDB, userHelp.LoadUserToRedis)
	userHelp.LoadModuleInit(mt.MerchantDB, myredis.SetUserFieldNotLab)

	//demo
	//userHelp.LoadUserToRedis("23")

	srv := &fasthttp.Server{
		Handler:            middleware.Use(app.Handler),
		ReadTimeout:        router.ApiTimeout,
		WriteTimeout:       router.ApiTimeout,
		Name:               "member",
		MaxRequestBodySize: 51 * 1024 * 1024,
	}

	fmt.Printf("gitReversion = %s\r\nbuildGoVersion = %s\r\nbuildTime = %s\r\n", pkg.GitReversion, pkg.BuildGoVersion, pkg.BuildTime)
	fmt.Println("member running", cfg.Port.Member)
	if err := srv.ListenAndServe(cfg.Port.Member); err != nil {
		log.Fatalf("Error in ListenAndServe: %s", err)
	}

}

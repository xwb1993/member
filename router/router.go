package router

import (
	"fmt"
	"runtime/debug"
	"time"

	"member/controller"

	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
)

var (
	ApiTimeoutMsg = `{"status": "false","data":"服务器响应超时，请稍后重试"}`
	ApiTimeout    = time.Second * 30
	route         *router.Router
	buildInfo     BuildInfo
)

type BuildInfo struct {
	GitReversion   string
	BuildTime      string
	BuildGoVersion string
}

func apiServerPanic(ctx *fasthttp.RequestCtx, rcv interface{}) {

	err := rcv.(error)
	fmt.Println(err)
	debug.PrintStack()

	if r := recover(); r != nil {
		fmt.Println("recovered failed", r)
	}

	ctx.SetStatusCode(500)
	return
}

func Version(ctx *fasthttp.RequestCtx) {

	ctx.SetContentType("text/html; charset=utf-8")
	fmt.Fprintf(ctx, "reportApi<br />Git reversion = %s<br />Build Time = %s<br />Go version = %s<br />System Time = %s<br />",
		buildInfo.GitReversion, buildInfo.BuildTime, buildInfo.BuildGoVersion, ctx.Time())

	//ctx.Request.Header.VisitAll(func (key, value []byte) {
	//	fmt.Fprintf(ctx, "%s: %s<br/>", string(key), string(value))
	//})
}

// SetupRouter 设置路由列表
func SetupRouter(b BuildInfo) *router.Router {

	route = router.New()
	route.PanicHandler = apiServerPanic
	buildInfo = b

	AddPayGroup()
	AddSmsGroup()
	AddMemberGroup()
	AddConfigGroup()
	AddProxyGroup()
	return route
}

func AddProxyGroup() {
	proxyCtl := new(controller.ProxyController)
	configGroup := route.Group("/Proxy")
	get(configGroup, "/accuInfo", proxyCtl.QueryAccuInfo)
	get(configGroup, "/QueryProxyCommi", proxyCtl.QueryWeekDepositReturn)
	get(configGroup, "/AchieveProxyCommi", proxyCtl.AchieveWeekDepositReturn)
}

func AddConfigGroup() {
	configGroup := route.Group("/Config")
	configCtl := new(controller.ConfigController)

	get(configGroup, "/weekDepositReturn", configCtl.WeekDepositReturn)
	get(configGroup, "/weekLostReturn", configCtl.WeekLostReturn)
	get(configGroup, "/weekRunningReturn", configCtl.WeekRunningReturn)
	get(configGroup, "/weekProxyCommiReturn", configCtl.WeekProxyCommiReturn)
	get(configGroup, "/accuDepositBonus", configCtl.AccuDepositBonus)

}

// 后期 AddMemberGroup 里面关于配置的可以调整到AddConfigGroup
func AddMemberGroup() {
	// app升级
	upgradeCtl := new(controller.UpgradeController)
	favCtl := new(controller.FavController)
	tagCtl := new(controller.TagController)

	memberCtl := new(controller.MemberController)
	//银行卡管理
	bankCtl := new(controller.BankcardController)
	bannerCtl := new(controller.BannerController)
	//vipCtl := new(controller.MemberVipController)
	noticeCtl := new(controller.NoticeController)
	msgCtl := new(controller.MessageController)
	signCtl := new(controller.PromoSignController)
	treasureCtl := new(controller.PromoTreasureController)
	recordCtl := new(controller.RecordController)
	pdCtl := new(controller.PromoDepositController)
	gameCtl := new(controller.GameController)
	reportCtl := new(controller.ReportController)
	csCtl := new(controller.CsController)
	weekly := new(controller.PromoWeeklyController)
	//转盘管理
	turnTableCtl := new(controller.TurnTableController)

	memberGroup := route.Group("/member")
	get(memberGroup, "/cs/list", csCtl.List)

	// app升级检测
	get(memberGroup, "/app/upgrade", upgradeCtl.Info)

	get(memberGroup, "/fav/delete", favCtl.Delete)
	get(memberGroup, "/fav/list", favCtl.List)
	get(memberGroup, "/fav/insert", favCtl.Insert)

	get(memberGroup, "/update/avatar", memberCtl.UpdateAvatar)
	get(memberGroup, "/logout", memberCtl.Logout)

	post(memberGroup, "/reg", memberCtl.Reg)

	get(memberGroup, "/tag/list", tagCtl.List)
	// 会员信息
	get(memberGroup, "/token", memberCtl.Token)
	get(memberGroup, "/info", memberCtl.Info)
	// 检测转盘开关是否开启
	get(memberGroup, "/turntable/switch", memberCtl.TurntableSwitch)
	// 检测会员账号是否可用
	get(memberGroup, "/valid", memberCtl.Valid)
	// 会员登陆
	post(memberGroup, "/login", memberCtl.Login)
	// 会员余额信息 中心钱包和锁定钱包
	get(memberGroup, "/balance", memberCtl.Balance)
	// 用户忘记密码
	post(memberGroup, "/password/forget", memberCtl.ForgetPassword)
	// 用户修改密码
	post(memberGroup, "/password/update", memberCtl.UpdatePassword)
	// 用户修改支付密码
	post(memberGroup, "/pay/password/update", memberCtl.UpdatePayPassword)
	// 绑定手机号
	post(memberGroup, "/bind/phone", memberCtl.BindPhone)
	// 绑定邮箱
	post(memberGroup, "/bind/email", memberCtl.BindEmail)

	post(memberGroup, "/update/info", memberCtl.UpdateInfo)
	//新增银行卡
	post(memberGroup, "/bankcard/insert", bankCtl.Insert)
	//查询银行卡
	get(memberGroup, "/bankcard/list", bankCtl.List)
	//查询银行类型
	get(memberGroup, "/banktype/list", bankCtl.BankType)
	// 导航栏列表
	get(memberGroup, "/lastwin", memberCtl.LastWinRecord)
	// 导航栏
	get(memberGroup, "/nav", memberCtl.Nav)

	get(memberGroup, "/game/hot/list", gameCtl.Hot)
	get(memberGroup, "/game/rec/list", gameCtl.Rec)
	get(memberGroup, "/game/fav/list", gameCtl.Fav)
	get(memberGroup, "/game/search", gameCtl.Search)

	get(memberGroup, "/game/list", gameCtl.List)

	// 站内信-列表
	get(memberGroup, "/message/list", msgCtl.List)
	// 站内信-读取
	post(memberGroup, "/message/read", msgCtl.Read)
	// 站内信-未读数
	get(memberGroup, "/message/num", msgCtl.Num)
	// 站内信-删除
	post(memberGroup, "/message/delete", msgCtl.Delete)

	// 广告列表
	get(memberGroup, "/banner", bannerCtl.List)
	// 公告列表
	get(memberGroup, "/notice", noticeCtl.List)

	// 签到活动-配置
	get(memberGroup, "/sign/config", signCtl.Config)
	// 签到活动-记录
	get(memberGroup, "/sign/record", signCtl.Record)
	// 签到活动-奖励记录
	get(memberGroup, "/sign/reward/record", signCtl.RewardRecord)
	// 签到活动-签到
	get(memberGroup, "/sign", signCtl.Sign)

	// 宝箱活动-配置
	get(memberGroup, "/treasure/config", treasureCtl.Config)
	// 宝箱活动-记录
	get(memberGroup, "/treasure/record", treasureCtl.Record)
	// 宝箱活动-申请
	get(memberGroup, "/treasure/apply", treasureCtl.Apply)

	// 周投注活动-配置
	get(memberGroup, "/weekly/config", weekly.Config)

	// 转盘信息
	get(memberGroup, "/turnTable/info", turnTableCtl.Info)
	// 转盘点击
	get(memberGroup, "/turnTable/click", turnTableCtl.Click)
	//奖励金额领取申请
	post(memberGroup, "/turnTable/apply", turnTableCtl.Apply)

	// 存款活动
	get(memberGroup, "/promo/deposit/config", pdCtl.Config)

	//充值提款订单
	get(memberGroup, "/record", recordCtl.Trade)
	//投注记录
	get(memberGroup, "/game/record", recordCtl.GameRecord)

	//奖励记录
	get(memberGroup, "/bonus/record", recordCtl.BonusRecord)
	// 邀请记录
	post(memberGroup, "/invite/record", recordCtl.InviteRecord)
	// 邀请明细总面板
	post(memberGroup, "/report/detail", reportCtl.Detail)
	// 邀请明细列表
	post(memberGroup, "/report/list", reportCtl.List)

	post(memberGroup, "/member/accu", memberCtl.MemberAccData)
	post(memberGroup, "/member/weekly/query", memberCtl.QueryWeekReturnInfo)
	post(memberGroup, "/member/weekly/deposit", memberCtl.AchieveDepoisutReturn)
	post(memberGroup, "/member/weekly/bet", memberCtl.AchieveBetReturn)
	post(memberGroup, "/member/weekly/lodst", memberCtl.AchieveLostReturn)

}
func AddSmsGroup() {
	smsCtl := new(controller.SmsController)
	smsGroup := route.Group("/sms")
	// 验证码
	post(smsGroup, "/send", smsCtl.Send)
	// 验证码
	post(smsGroup, "/send/mail", smsCtl.SendMail)
	// 验证码
	post(smsGroup, "/send/online", smsCtl.SendOnline)
	// 验证码
	post(smsGroup, "/send/online/mail", smsCtl.SendOnlineMail)
	//kmi短信回调
	get(smsGroup, "/callback/kmi", smsCtl.CallbackKmi)
}
func AddPayGroup() {
	payCtl := new(controller.ThirdController)

	payGroup := route.Group("/pay")
	//充值通道
	get(payGroup, "/channel", payCtl.DepositChannel)
	//充值
	post(payGroup, "/deposit", payCtl.Deposit)
	//提现配置
	get(payGroup, "/withdraw/config", payCtl.WithdrawConfig)
	//提现
	post(payGroup, "/withdraw", payCtl.Withdraw)

	//回调
	post(payGroup, "/callback", payCtl.CallbackEpay)

	//提现回调
	post(payGroup, "/callback/ew", payCtl.CallbackEpayW)
}

// get is a shortcut for router.GET(path string, handle fasthttp.RequestHandler)
func get(g *router.Group, path string, handle fasthttp.RequestHandler) {
	g.GET(path, fasthttp.TimeoutHandler(handle, ApiTimeout, ApiTimeoutMsg))
}

// post is a shortcut for router.POST(path string, handle fasthttp.RequestHandler)
func post(g *router.Group, path string, handle fasthttp.RequestHandler) {
	g.POST(path, fasthttp.TimeoutHandler(handle, ApiTimeout, ApiTimeoutMsg))
}

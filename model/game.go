package model

import (
	myUserHelp "common/userHelp"
	"errors"
	"fmt"
	"github.com/go-redis/redis/v8"
	"member/contrib/helper"

	"github.com/meilisearch/meilisearch-go"
	"github.com/valyala/fasthttp"
)

func GameFullTextMeili(gameType, page, pageSize int, platformId, tagId, word string) (GameData, error) {

	var data GameData

	offset_t := pageSize * (page - 1)

	index := meta.Meili.Index("games")

	cond := &meilisearch.SearchRequest{
		Limit:  int64(pageSize),
		Offset: int64(offset_t),
	}

	filter := "online = 1"
	if gameType > 0 {
		filter += fmt.Sprintf(" AND game_type = %d", gameType)
	}
	if platformId != "0" {
		filter += fmt.Sprintf(" AND platform_id = %s", platformId)
	}
	if tagId != "0" {
		filter += fmt.Sprintf(" AND tag_ids = %s", tagId)
	}

	cond.Filter = filter
	searchRes, err := index.Search(word, cond)
	if err != nil {
		return data, err
	}
	ll := len(searchRes.Hits)
	if ll == 0 {
		data.S = 0
		data.T = 0
		return data, err
	}
	data.D = make([]Game, ll)
	data.T = searchRes.EstimatedTotalHits
	data.S = uint(pageSize)

	for i, v := range searchRes.Hits {
		val := v.(map[string]interface{})

		data.D[i].ID = val["id"].(string)
		data.D[i].PlatformID = val["platform_id"].(string)
		data.D[i].Name = ""
		data.D[i].EnName = val["en_name"].(string)
		data.D[i].BrAlias = val["br_alias"].(string)
		data.D[i].ClientType = val["client_type"].(string)
		data.D[i].GameType = int(val["game_type"].(float64))
		data.D[i].GameID = val["game_id"].(string)
		data.D[i].Img = val["img"].(string)
		data.D[i].Online = int(val["online"].(float64))
		data.D[i].IsHot = int(val["is_hot"].(float64))
		data.D[i].IsFav = int(val["is_fav"].(float64))
		data.D[i].IsNew = int(val["is_new"].(float64))

	}

	return data, nil

}

func GameListMeili(fctx *fasthttp.RequestCtx, gameType, hot, page, pageSize int, platformId, field string) (GameData, error) {

	var (
		data GameData
		gids []interface{}
	)

	offset_t := pageSize * (page - 1)

	index := meta.Meili.Index("games")

	cond := &meilisearch.SearchRequest{
		Limit:  int64(pageSize),
		Offset: int64(offset_t),
		Sort:   []string{"sorting:asc"},
	}

	filter := "online = 1"
	if gameType > 0 {
		filter += fmt.Sprintf(" AND game_type = %d AND %s = 1", gameType, field)
	} else {
		filter += fmt.Sprintf(" AND %s = 1", field)
	}

	if platformId != "0" {
		filter += fmt.Sprintf(" AND platform_id = %s", platformId)
	}

	if hot > 0 {
		filter += fmt.Sprintf(" AND is_hot = %d", hot)
	}

	//fmt.Println("GameListMeili filter = ", filter)
	cond.Filter = filter
	searchRes, err := index.Search("", cond)
	if err != nil {
		return data, err
	}

	ll := len(searchRes.Hits)
	if ll == 0 {
		return data, err
	}

	gids = make([]interface{}, ll)
	data.D = make([]Game, ll)
	data.T = searchRes.EstimatedTotalHits
	data.S = uint(pageSize)

	for i, v := range searchRes.Hits {
		val := v.(map[string]interface{})

		data.D[i].ID = val["id"].(string)
		data.D[i].PlatformID = val["platform_id"].(string)
		data.D[i].Name = ""
		data.D[i].EnName = val["en_name"].(string)
		data.D[i].BrAlias = val["br_alias"].(string)
		data.D[i].ClientType = val["client_type"].(string)
		data.D[i].GameType = int(val["game_type"].(float64))
		data.D[i].GameID = val["game_id"].(string)
		data.D[i].Img = val["img"].(string)
		data.D[i].Online = int(val["online"].(float64))
		data.D[i].IsHot = int(val["is_hot"].(float64))
		data.D[i].IsFav = 2
		data.D[i].IsNew = int(val["is_new"].(float64))

		gids[i] = val["id"].(string)
	}

	uid := GetUidFromToken(fctx)
	if uid == "" {
		return data, errors.New(helper.AccessTokenExpires)
	}

	member, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		return data, errors.New(helper.UserNotExist)
	}

	if ret {
		pipe := meta.MerchantRedis.Pipeline()
		for _, gid := range gids {
			key := fmt.Sprintf("fav:%s", member.Uid)
			pipe.SIsMember(ctx, key, gid)
		}
		res, err := pipe.Exec(ctx)
		if err != nil {
			// 错误处理
		}
		for i, r := range res {
			if r.(*redis.BoolCmd).Val() {
				data.D[i].IsFav = 1
			}
		}
		//key := fmt.Sprintf("fav:%s", member.Uid)
		//res := meta.MerchantRedis.SMIsMember(ctx, key, gids...).Val()
		////fmt.Println("res = ", res)
		//for i := 0; i < ll; i++ {
		//	if res[i] {
		//		data.D[i].IsFav = 1
		//	}
		//}
	}

	return data, nil
}

func GameList(fctx *fasthttp.RequestCtx, page, pageSize int, filter string) (GameData, error) {

	var (
		data GameData
		gids []interface{}
	)

	offset_t := pageSize * (page - 1)

	index := meta.Meili.Index("games")

	cond := &meilisearch.SearchRequest{
		Limit:  int64(pageSize),
		Offset: int64(offset_t),
	}

	//fmt.Println("GameList filter = ", filter)
	cond.Filter = filter
	searchRes, err := index.Search("", cond)
	if err != nil {
		return data, err
	}
	ll := len(searchRes.Hits)
	if ll == 0 {
		data.S = 0
		data.T = 0
		return data, err
	}

	gids = make([]interface{}, ll)
	data.D = make([]Game, ll)
	data.T = searchRes.EstimatedTotalHits
	data.S = uint(pageSize)

	for i, v := range searchRes.Hits {
		val := v.(map[string]interface{})

		data.D[i].ID = val["id"].(string)
		data.D[i].PlatformID = val["platform_id"].(string)
		data.D[i].Name = ""
		data.D[i].EnName = val["en_name"].(string)
		data.D[i].BrAlias = val["br_alias"].(string)
		data.D[i].ClientType = val["client_type"].(string)
		data.D[i].GameType = int(val["game_type"].(float64))
		data.D[i].GameID = val["game_id"].(string)
		data.D[i].Img = val["img"].(string)
		data.D[i].Online = int(val["online"].(float64))
		data.D[i].IsHot = int(val["is_hot"].(float64))
		data.D[i].IsFav = 2
		data.D[i].IsNew = int(val["is_new"].(float64))

		gids[i] = val["id"].(string)
	}

	uid := GetUidFromToken(fctx)
	if uid == "" {
		return data, errors.New(helper.AccessTokenExpires)
	}

	member, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		return data, errors.New(helper.UserNotExist)
	}

	if err == nil {
		pipe := meta.MerchantRedis.Pipeline()
		for _, gid := range gids {
			key := fmt.Sprintf("fav:%s", member.Uid)
			pipe.SIsMember(ctx, key, gid)
		}
		res, err := pipe.Exec(ctx)
		if err != nil {
			// 错误处理
		}
		for i, r := range res {
			if r.(*redis.BoolCmd).Val() {
				data.D[i].IsFav = 1
			}
		}
	}

	return data, nil
}

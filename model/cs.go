package model

func CsList(field []string) (map[string]string, error) {

	value := map[string]string{}

	res, err := meta.MerchantRedis.HMGet(ctx, "cs", field...).Result()
	if err != nil {
		return value, err
	}

	for i, val := range field {
		if len(res) > 0 {
			if v, ok := res[i].(string); ok {
				value[val] = v
			}
		} else {
			value[val] = ""
		}
	}

	return value, nil
}

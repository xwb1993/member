package model

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
	"io/ioutil"
	"member/contrib/helper"
	"member/contrib/validator"
	"net/http"
	"strconv"
	"strings"
)

type thirdEpay struct {
}

//type EpayCallBack struct {
//	MchId      int    `json:"mch_id"`
//	Code       int    `json:"code"`
//	OrderNo    string `json:"order_no"`
//	DisOrderNo string `json:"dis_order_no"`
//	OrderPrice int    `json:"order_price"`
//	RealPrice  int    `json:"real_price"`
//	OrderCost  int    `json:"order_cost"`
//	NtiTime    int    `json:"nti_time"`
//	Sign       string `json:"sign"`
//}

type EpayCallBack struct {
	AppId           string `json:"appId"`
	CustId          string `json:"custId"`
	MerchantOrderId string `json:"merchantOrderId"`
	Order           string `json:"order"`
	OrderStatus     string `json:"orderStatus"`
	Amount          string `json:"amount"`
	OrderTime       string `json:"orderTime"`
	Sign            string `json:"sign"`
}

type EpayWithdrawCallBack struct {
	OrderNo    string `json:"order_no"`
	MchOrderNo string `json:"mch_order_no"`
	Amount     int64  `json:"amount"`
	Status     int    `json:"status"`
	Fee        int    `json:"fee"`
	Sign       string `json:"sign"`
}

type depositResp struct {
	PayType         string `json:"payType"`
	PayContent      string `json:"payContent"`
	Order           string `json:"order"`
	MerchantOrderId string `json:"merchantOrderId"`
	Code            string `json:"code"`
	Msg             string `json:"msg"`
	ContentType     string `json:"contentType"`
	Sign            string `json:"sign"`
}

type withdrawResp struct {
	Code            string `json:"code"`
	Msg             string `json:"msg"`
	OrdStatus       string `json:"ordStatus"`
	MerchantOrderId string `json:"merchantOrderId"`
	Amount          string `json:"amount"`
	Time            string `json:"time"`
	Order           string `json:"order"`
	Sign            string `json:"sign"`
}

func (that *thirdEpay) DepositCallBack(fctx *fasthttp.RequestCtx) (EpayCallBack, error) {

	//m := map[string]string{}
	params := EpayCallBack{}
	fmt.Println("depositReq===", string(fctx.PostBody()))
	//if err := helper.JsonUnmarshal(fctx.PostBody(), &params); err != nil {
	//	fmt.Println("epay param format err:", err.Error())
	//	return m, errors.New(helper.ParamErr)
	//}

	params.AppId = string(fctx.FormValue("appId"))
	params.CustId = string(fctx.FormValue("custId"))
	params.MerchantOrderId = string(fctx.FormValue("merchantOrderId"))
	params.Order = string(fctx.FormValue("order"))
	params.OrderStatus = string(fctx.FormValue("orderStatus"))
	params.Amount = string(fctx.FormValue("amount"))
	params.OrderTime = string(fctx.FormValue("orderTime"))
	params.Sign = string(fctx.FormValue("sign"))
	//m["order_id"] = params.OrderId
	//m["amount"] = params.Amount
	//m["success_time"] = params.Datetime
	//if params.ReturnCode != "00" {
	//	m["state"] = fmt.Sprintf(`%d`, DepositCancelled)
	//} else {
	//	m["state"] = fmt.Sprintf(`%d`, DepositSuccess)
	//}
	fmt.Println("params===", params)
	return params, nil
	//return m, nil
}

func (that *thirdEpay) Withdraw(amount, orderNo, accountNo, uid, pixId, flag string, p TblPayFactory) error {

	var (
		//iamount     int64
		accountType = int64(0)
		//pixType     string
	)
	//if flag == "1" {
	//	pixType = "CPF"
	//	accountType = 1007
	//} else if flag == "2" {
	//	pixType = "PHONE"
	//	accountNo = "+55" + accountNo
	//} else if flag == "3" {
	//	pixType = "EMAIL"
	//}

	//if validator.CtypeDigit(amount) {
	//	iamount, _ = strconv.ParseInt(amount, 10, 64)
	//}
	//dp, err := withdrawEpayOrder(accountType, iamount*100, p.Mchid, accountNo, accountName, pixId, pixType, p.Url, p.AppKey, orderNo, meta.Callback+"/ew")
	err := withdrawEpayOrder(accountType, amount, accountNo, uid, pixId, "CPF", orderNo, p)
	if err != nil {
		fmt.Println("err:", err)

	}
	return nil
}

func (that *thirdEpay) WithdrawCallBack(fctx *fasthttp.RequestCtx) (paymentCallbackResp, error) {

	data := paymentCallbackResp{}
	//params := EpayWithdrawCallBack{}
	//if err := helper.JsonUnmarshal(fctx.PostBody(), &data); err != nil {
	//	fmt.Println("epay param format err:", err.Error())
	//	return data, errors.New(helper.ParamErr)
	//}
	fmt.Println("withdrawReq===", string(fctx.PostBody()))
	data.AppId = string(fctx.FormValue("appId"))
	data.CustId = string(fctx.FormValue("custId"))
	data.MerchantOrderId = string(fctx.FormValue("merchantOrderId"))
	data.Order = string(fctx.FormValue("order"))
	data.OrderStatus = string(fctx.FormValue("orderStatus"))
	data.Amount = string(fctx.FormValue("amount"))
	data.Sign = string(fctx.FormValue("sign"))
	fmt.Println("WithdrawCallBack:", data)

	content := fmt.Sprintf(`amount=%s&appId=%s&custId=%s&merchantOrderId=%s&order=%s&orderStatus=%s&key=%s`, data.Amount, data.AppId, data.CustId, data.MerchantOrderId, data.Order, data.OrderStatus, meta.TgPay.AppKey)
	fmt.Println(content)
	sign := strings.ToUpper(helper.MD5Hash(content))
	fmt.Println("wdcb_sign==", sign)
	if data.Sign != sign {
		fmt.Printf("old sign=%s, new state=%s", data.Sign, sign)
		return data, errors.New(helper.SignValidErr)
	}
	return data, nil
}

//func (that *thirdEpay) Deposit(fctx *fasthttp.RequestCtx, username, amount, orderNo string, p TblPayFactory) (PaymentDepositResp, error) {
//	//ip := helper.FromRequest(fctx)
//
//	var (
//		iamount int64
//	)
//	//if validator.CtypeDigit(p.Mchid) {
//	//	mid, _ = strconv.ParseInt(p.Mchid, 10, 64)
//	//}
//	//if validator.CtypeDigit(p.AppId) {
//	//	appid, _ = strconv.ParseInt(p.AppId, 10, 64)
//	//}
//	//if validator.CtypeDigit(p.PayCode) {
//	//	paycode, _ = strconv.ParseInt(p.PayCode, 10, 64)
//	//}
//	if validator.CtypeDigit(amount) {
//		iamount, _ = strconv.ParseInt(amount, 10, 64)
//	}
//
//	//if p.Ty == 2 {
//	//	pr := decimal.NewFromFloat(p.PayRate)
//	//	iamount = decimal.NewFromInt(iamount).Div(pr).Mul(decimal.NewFromInt(100)).IntPart()
//	//} else {
//	//	iamount = iamount * 100
//	//}
//
//	return CreateEpayOrder(iamount, p.Mchid, p.PayCode, p.Url, p.AppKey, orderNo, username, meta.Callback, meta.WebUrl)
//}

func (that *thirdEpay) CreateEpayOrder(iamount, orderId, uid string, pay TblPayFactory) (PaymentDepositResp, error) {

	var amount int64
	if validator.CtypeDigit(iamount) {
		amount, _ = strconv.ParseInt(iamount, 10, 64)
	}
	amount = amount * 100
	resp := PaymentDepositResp{
		OrderID: orderId,
	}
	//amount := strconv.FormatInt(money, 10)
	resp.UsdtCount, _ = decimal.NewFromInt(amount).Div(decimal.NewFromInt(100)).Float64()
	//ts := time.Now().Format(time.DateTime)
	//content := fmt.Sprintf(`pay_amount=%s&pay_applydate=%s&pay_bankcode=%s&pay_callbackurl=%s&pay_memberid=%s&pay_notifyurl=%s&pay_orderid=%s&pay_userid=%s&key=%s`, fmt.Sprintf("%.2f", float64(amount)), ts, payCode, payJumpUrl, mchid, payNoticeUrl, orderId, uid, appKey)
	content := fmt.Sprintf(`amount=%d&appId=%s&backUrl=%s&countryCode=%s&currencyCode=%s&custId=%s&merchantOrderId=%s&remark=%s&type=%s&userName=%s&key=%s`, amount, pay.AppId, pay.NotifyUrl, pay.CountryCode, pay.CurrencyCode, pay.Mchid, orderId, pay.PayCode, pay.Type, uid, pay.AppKey)
	fmt.Println(content)
	sign := strings.ToUpper(helper.MD5Hash(content))
	fmt.Println(sign)

	requestURI := fmt.Sprintf("%s", pay.Url)
	fmt.Println(requestURI)

	recs := map[string]interface{}{
		"amount":          amount,
		"appId":           pay.AppId,
		"backUrl":         pay.NotifyUrl,
		"countryCode":     pay.CountryCode,
		"currencyCode":    pay.CurrencyCode,
		"merchantOrderId": orderId,
		"remark":          pay.PayCode,
		"sign":            sign,
		"custId":          pay.Mchid,
		"type":            pay.Type,
		"userName":        uid,
	}
	requestBody, err := helper.JsonMarshal(recs)
	if err != nil {
		return resp, errors.New(helper.FormatErr)
	}
	fmt.Println(string(requestBody))

	client := &http.Client{}
	req, err := http.NewRequest("POST", requestURI, bytes.NewReader(requestBody))
	if err != nil {
		return resp, errors.New(helper.PayServerErr)
	}
	//req.Header.Add("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")
	req.Header.Add("Content-Type", "application/json")
	res, err := client.Do(req)
	if err != nil {
		return resp, errors.New(helper.PayServerErr)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return resp, errors.New(helper.PayServerErr)
	}
	fmt.Println(string(body))
	var data depositResp
	err = json.Unmarshal(body, &data)
	if err != nil {
		return resp, fmt.Errorf(helper.PayServerErr)
	}

	//var p fastjson.Parser
	//v, err := p.ParseBytes(body)
	//if err != nil {
	//	return resp, fmt.Errorf(helper.PayServerErr)
	//}

	fmt.Println("data===", data)
	if data.Code != "000000" {
		return resp, fmt.Errorf(helper.PayServerErr)
	}
	resp.Addr = data.PayContent
	//resp.Addr = string(d.GetStringBytes("payurl"))
	//fmt.Println("payurl===", resp.Addr)
	//resp.OrderInfo = string(d.GetStringBytes("orderInfo"))
	//resp.Sign = string(d.GetStringBytes("sign"))

	return resp, nil
}

func withdrawEpayOrder(accountType int64, iamount, accountNo, uid, pixId, pixType, orderId string, pay TblPayFactory) error {
	//resp := paymentWithdrawResp{
	//	OrderID: orderId,
	//}
	var amount int64
	if validator.CtypeDigit(iamount) {
		amount, _ = strconv.ParseInt(iamount, 10, 64)
	}
	amount = amount * 100
	content := fmt.Sprintf(`amount=%d&appId=%s&backUrl=%s&cardType=%s&countryCode=%s&cpf=%s&currencyCode=%s&custId=%s&email=%s&merchantOrderId=%s&phone=%s&remark=%s&type=%s&userName=%s&walletId=%s&key=%s`, amount, pay.AppId, pay.NotifyUrl, pixType, pay.CountryCode, accountNo, pay.CurrencyCode, pay.Mchid, "email", orderId, "phone", pay.PayCode, "PIX", uid, pixId, pay.AppKey)
	fmt.Println(content)
	sign := strings.ToUpper(helper.MD5Hash(content))
	fmt.Println(sign)
	requestURI := fmt.Sprintf("%s", pay.Url)
	fmt.Println(requestURI)

	recs := map[string]interface{}{
		"amount":          amount,
		"appId":           pay.AppId,
		"backUrl":         pay.NotifyUrl,
		"cardType":        pixType,
		"countryCode":     pay.CountryCode,
		"cpf":             accountNo,
		"currencyCode":    pay.CurrencyCode,
		"custId":          pay.Mchid,
		"email":           "email",
		"merchantOrderId": orderId,
		"phone":           "phone",
		"remark":          pay.PayCode,
		"type":            "PIX",
		"userName":        uid,
		"walletId":        pixId,
		"sign":            sign,
	}
	requestBody, err := helper.JsonMarshal(recs)
	if err != nil {
		return errors.New(helper.FormatErr)
	}
	fmt.Println(string(requestBody))
	//payload := strings.NewReader(fmt.Sprintf(`&amount=%s&appId=%s&backUrl=%s&cardType=%s&countryCode=%s&cpf=%s&currencyCode=%s&custId=%s&email=%s&merchantOrderId=%s&phone=%s&remark=%s&type=%s&userName=%s&walletId=%s&sign=%s`, amount, pay.AppId, pay.NotifyUrl, pixType, pay.CountryCode, accountNo, pay.CurrencyCode, pay.Mchid, "email", orderId, "phone", pay.PayCode, "PIX", username, pixId, sign))

	client := &http.Client{}
	req, err := http.NewRequest("POST", requestURI, bytes.NewReader(requestBody))

	if err != nil {
		fmt.Println("这里报错1")
		return errors.New(helper.PayServerErr)
	}
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		fmt.Println("这里报错2")
		return errors.New(helper.PayServerErr)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println("这里报错3")
		return errors.New(helper.PayServerErr)
	}
	fmt.Println(string(body))

	var data withdrawResp
	err = json.Unmarshal(body, &data)
	if err != nil {
		return fmt.Errorf(helper.PayServerErr)
	}
	fmt.Println("data===", data)
	fmt.Println("code===", data.Code)
	if data.Code != "000000" {
		return fmt.Errorf(helper.PayServerErr)
	}

	return nil
}

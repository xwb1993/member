package model

import (
	myConfig "common/config"
	myredis "common/redis"
	myUserHelp "common/userHelp"
	"database/sql"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"member/contrib/helper"
	"time"
)

type TblMemberBankcard struct {
	Id         string `json:"id" db:"id" cbor:"id"`
	Uid        string `json:"uid" db:"uid" cbor:"uid"`
	Username   string `json:"username" db:"username" cbor:"username"`
	CreatedAt  int    `json:"created_at" db:"created_at" cbor:"created_at"`
	State      int    `json:"state" db:"state" cbor:"state"` //1=正常,2=停用,3=黑名单'
	PixAccount string `json:"pix_account" db:"pix_account" cbor:"pix_account"`
	Flag       int    `json:"flag" db:"flag" cbor:"flag"` //1 cpf 2 phone 3 email
	RealName   string `json:"real_name" db:"real_name" cbor:"real_name"`
	PixId      string `json:"pix_id" db:"pix_id" cbor:"pix_id"`
}

type memberBankData struct {
	T int                 `json:"t"`
	D []TblMemberBankcard `json:"d"`
}

type TblBanktype struct {
	Id       int    `json:"id" db:"id" cbor:"id"`
	Bankname string `json:"bankname" db:"bankname" cbor:"bankname"`
	Bankcode string `json:"bankcode" db:"bankcode" cbor:"bankcode"`
	State    int    `json:"state" db:"state" cbor:"state"`
}

type bankTypeData struct {
	T int           `cbor:"t"`
	D []TblBanktype `cbor:"d"`
}

func BankcardInsert(uid, username, realName, flag, pixAccount, pixId, bankCode, payPassword string) error {

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(uid)

	if !ret {
		return errors.New(helper.UserNotExist)
	}

	if mb.PayPassword == "0" || mb.PayPassword == "" {
		return errors.New(helper.SetWithdrawPwdFirst)
	}

	if payPassword == "" {
		return errors.New(helper.WithdrawPwdMismatch)
	}

	//pwd := fmt.Sprintf("%d", MurmurHash(payPassword, mb.CreatedAt))
	if payPassword != mb.PayPassword {
		return errors.New(helper.WithdrawPwdMismatch)
	}

	// 判断会员银行卡数目
	if mb.BankcardTotal >= 5 {
		return errors.New(helper.MaxThreeBankCard)
	}

	err := BankCardExistRedis(pixId)
	if err != nil {
		return err
	}

	// 会员银行卡插入加锁
	lKey := fmt.Sprintf("bc:%s", username)
	err = Lock(lKey)
	defer Unlock(lKey)
	if err != nil {
		return err
	}

	bankType, err := bankNameByCode(bankCode)
	if err != nil {
		return errors.New(helper.BankNameOrCodeErr)
	}

	//开启事务
	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	bankcardRecord := g.Record{
		"id":          helper.GenId(),
		"uid":         uid,
		"username":    username,
		"pix_id":      pixId,
		"pix_account": pixAccount,
		"flag":        flag,
		"real_name":   realName,
		"created_at":  time.Now().Unix(),
		"bankcode":    bankCode,
		"bankname":    bankType.Bankname,
	}
	// 更新会员银行卡信息
	query, _, _ := dialect.Insert("tbl_member_bankcard").Rows(bankcardRecord).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	myredis.AddUserFieldValueByInt64(mb.Uid, myConfig.G_tbl_member_base+"bankcard_total", 1)

	_ = tx.Commit()

	key := "merchant:bankcard_exist"
	_ = meta.MerchantRedis.SAdd(ctx, key, pixId).Err()
	return nil
}

func BankCardExistRedis(bankcardNo string) error {

	pipe := meta.MerchantRedis.Pipeline()
	defer pipe.Close()

	key1 := "merchant:bankcard_exist"
	key2 := "merchant:bankcard_blacklist"

	ex1Temp := pipe.SIsMember(ctx, key1, bankcardNo)

	ex2Temp := pipe.SIsMember(ctx, key2, bankcardNo)
	_, err := pipe.Exec(ctx)
	if err != nil {
		return pushLog(err, helper.RedisErr)
	}

	if ex1Temp.Val() {
		return errors.New(helper.BankCardExistErr)
	}

	if ex2Temp.Val() {
		return errors.New(helper.BankcardBan)
	}
	return nil
}

func BankcardList(uid string) (memberBankData, error) {

	var (
		data memberBankData
	)

	query, _, _ := dialect.From("tbl_member_bankcard").Select(colsBankcard...).Where(g.Ex{"uid": uid}).ToSQL()
	//fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}
	data.T = len(data.D)

	return data, nil
}

func BankcardFindOne(id string) (TblMemberBankcard, error) {

	var (
		data TblMemberBankcard
	)

	query, _, _ := dialect.From("tbl_member_bankcard").Select(colsBankcard...).Where(g.Ex{"id": id}).Limit(1).ToSQL()
	//fmt.Println(query)
	err := meta.MerchantDB.Get(&data, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}

func BanktypeList() (bankTypeData, error) {

	var (
		data bankTypeData
	)

	query, _, _ := dialect.From("tbl_banktype").Select(colsBankType...).Where(g.Ex{"state": 1}).ToSQL()
	//fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	data.T = len(data.D)

	return data, nil
}

func bankNameByCode(bankcode string) (TblBanktype, error) {

	var (
		data TblBanktype
	)

	query, _, _ := dialect.From("tbl_banktype").Select(colsBankType...).Where(g.Ex{"bankcode": bankcode}).Limit(1).ToSQL()
	//fmt.Println(query)
	err := meta.MerchantDB.Get(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return data, errors.New(helper.RecordNotExistErr)
	}

	return data, nil
}

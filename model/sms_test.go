package model

import (
	"fmt"
	"testing"
)

func TestBuka(t *testing.T) {

	New("0.0.0.0")

	err := smsBuka("55856321413", "619761")
	if err != nil {
		fmt.Println("send sms error:", err)
	} else {
		fmt.Println("send sms success")
	}
}

func TestEmail(t *testing.T) {

	from := "lavabit.com"
	to := "xyy6699@gmail.com"
	subj := "This is the email subject"
	body := "This is an example body.\n With two lines."
	host := "lavabit.com"
	port := 25587
	password := ""

	SendMail(from, to, host, port, password, subj, body)
}

func TestKmi(t *testing.T) {

	New("0.0.0.0")

	err := smsKmi("005511917760355", "200001")
	if err != nil {
		fmt.Println("send sms error:", err)
	} else {
		fmt.Println("send sms success")
	}
}

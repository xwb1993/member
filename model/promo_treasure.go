package model

import (
	myConfig "common/config"
	myredis "common/redis"
	myUserHelp "common/userHelp"
	"database/sql"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"member/contrib/helper"
	"time"
)

// 签到活动配置
func PromoTreasureConfigList() ([]PromoTreasureConfig, error) {

	var data []PromoTreasureConfig
	query, _, _ := dialect.From("tbl_promo_treasure_config").
		Select(colsPromoTreasureConfig...).Where(g.Ex{}).Order(g.C("invite_num").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}

// 签到记录
func PromoTreasureMemberRecord(uid string) ([]PromoTreasureRecord, error) {

	var data []PromoTreasureRecord
	query, _, _ := dialect.From("tbl_promo_treasure_record").
		Select(colsPromoTreasureRecord...).Where(g.Ex{"uid": uid}).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}

// 签到记录
func PromoTreasureApply(mb myUserHelp.TblMemberBase, inviteNum int) error {

	if mb.LastTreasure == inviteNum {
		return errors.New(helper.DuplicateApplyErr)
	}

	if mb.LastTreasure > inviteNum {
		return errors.New(helper.ApplySequenceErr)
	}

	data := PromoTreasureConfig{}
	query, _, _ := dialect.From("tbl_promo_treasure_config").
		Select(colsPromoTreasureConfig...).Where(g.Ex{"invite_num": inviteNum}).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return errors.New(helper.ParamErr)
	}

	b, ret := myUserHelp.GetMemberBalanceInfo(mb.Uid)
	if !ret {
		return errors.New(helper.UserNotExist)
	}

	// 执行签到流程
	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	ts := time.Now()
	// 增加会员余额
	amount, _ := decimal.NewFromString(data.Amount)
	treasureRecord := PromoTreasureRecord{
		ID:        helper.GenId(),
		UID:       mb.Uid,
		Username:  mb.Username,
		InviteNum: inviteNum,
		Amount:    amount.String(),
		CreatedAt: ts.Unix(),
	}

	balance := decimal.NewFromFloat(b.Brl)
	balanceAfter := balance.Add(amount)
	id := helper.GenId()
	trans := MemberTransaction{
		AfterAmount:  balanceAfter.String(),
		Amount:       amount.String(),
		BeforeAmount: balance.String(),
		BillNo:       id,
		CreatedAt:    ts.UnixMilli(),
		ID:           id,
		CashType:     helper.TransactionTreasureDividend,
		UID:          mb.Uid,
		Username:     mb.Username,
		Remark:       fmt.Sprintf("invite num [%d], bonus amount [%s]", inviteNum, amount.String()),
	}
	query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	myredis.AddLogToRedis(query)

	if meta.WalletMode == "1" {
		myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", amount.Abs())
	} else if meta.WalletMode == "2" {
		myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", amount.Abs())
		myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"unlock_amount", amount.Abs())
	}

	myredis.SetUserFieldValueByInt(mb.Uid, myConfig.G_tbl_member_base+"last_treasure", inviteNum)

	query, _, _ = dialect.Insert("tbl_promo_treasure_record").Rows(treasureRecord).ToSQL()
	myredis.AddLogToRedis(query)

	err = tx.Commit()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	return nil
}

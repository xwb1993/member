package model

// 结构体定义

type MBBalance struct {
	Brl        string `db:"brl" json:"brl" cbor:"brl"`                         //余额
	LockAmount string `db:"lock_amount" json:"lock_amount" cbor:"lock_amount"` //锁定额度
}

type DepositRsp struct {
	QrCode      string `json:"qr_code" cbor:"qr_code"`
	Amount      string `json:"amount" cbor:"amount"`
	Account     string `json:"account" cbor:"account"`
	BankCode    string `json:"bank_code" cbor:"bank_code"`
	BankLogo    string `json:"bank_logo" cbor:"bank_logo"`
	CardHolder  string `json:"card_holder" cbor:"card_holder"`
	OrderNo     string `json:"order_no" cbor:"order_no"`
	PayCode     string `json:"pay_code" cbor:"pay_code"`
	ChannelType string `json:"channel_type" cbor:"channel_type"`
	IsRedirect  bool   `json:"is_redirect" cbor:"is_redirect"`
	RedirectURL string `json:"redirect_url" cbor:"redirect_url"`
}

type WithdrawRsp struct {
	BillNo string `json:"bill_no" cbor:"bill_no"`
	State  int    `json:"state" cbor:"state"`
}

type TblPayFactory struct {
	Fid          string          `json:"fid" db:"fid" redis:"fid" cbor:"fid"`
	Name         string          `json:"name" db:"name" redis:"name" cbor:"name"`
	Url          string          `json:"url" db:"url" redis:"url"`
	NotifyUrl    string          `json:"notify_url" db:"notify_url" redis:"notify_url"`
	Key          string          `json:"key" db:"key" redis:"key"`
	Mchid        string          `json:"mchid" db:"mchid" redis:"mchid"`
	AppId        string          `json:"app_id" db:"app_id" redis:"app_id"`
	AppKey       string          `json:"app_key" db:"app_key" redis:"app_key"`
	PayCode      string          `json:"pay_code" db:"pay_code" redis:"pay_code"`
	Fmax         string          `json:"fmax" db:"fmax" redis:"fmax" cbor:"fmax"`
	Fmin         string          `json:"fmin" db:"fmin" redis:"fmin" cbor:"fmin"`
	AmountList   string          `json:"amount_list" db:"amount_list" redis:"amount_list" cbor:"amount_list"`
	ShowName     string          `json:"show_name" db:"show_name" cbor:"show_name"`
	AmountArray  []DepositAmount `json:"amount_array" db:"-" cbor:"amount_array"`
	PayRate      float64         `json:"pay_rate" db:"pay_rate" cbor:"pay_rate"`
	Ty           int             `json:"ty" db:"ty" cbor:"ty"`
	Automatic    float64         `json:"automatic" db:"automatic" cbor:"automatic"`
	Sort         int             `json:"sort" cbor:"sort" db:"sort"`
	CountryCode  string          `json:"country_code" db:"country_code" cbor:"country_code"`
	CurrencyCode string          `json:"currency_code" db:"currency_code" cbor:"currency_code"`
	Type         string          `json:"type" db:"type" cbor:"type"`
}

type DepositAmount struct {
	Amount   string ` cbor:"amount" json:"amount"`
	Discount string `cbor:"discount" json:"discount"`
}

type TblPayFactoryData struct {
	T int             `json:"t" cbor:"t"`
	D []TblPayFactory `json:"d" cbor:"d"`
	R float64         `json:"r" cbor:"r"`
	C string          `json:"c" cbor:"c"`
}

// paymentDepositResp 存款
type PaymentDepositResp struct {
	Addr      string  `json:"addr" cbor:"addr"`           // 三方返回的充值地址
	OrderInfo string  `json:"orderInfo" cbor:"orderInfo"` // 支付订单信息
	Sign      string  `json:"sign" cbor:"sign"`           // MD5签名
	QrCode    string  `json:"qr_code" cbor:"qr_code"`     // 充值二维码地址
	OrderID   string  `json:"order_id" cbor:"order_id"`   //我方订单号
	Oid       string  `json:"oid" cbor:"oid"`             //三方的订单号
	UsdtCount float64 `json:"usdt_count"`                 //usdt个数
}

type MemberVip struct {
	Vip                  int     `json:"vip" db:"vip" cbor:"vip"`                                           //会员等级
	Name                 string  `json:"name" db:"name" cbor:"name"`                                        //会员等级名称
	DepositAmount        int64   `json:"deposit_amount" db:"deposit_amount" cbor:"deposit_amount"`          //对应的积分
	Flow                 int64   `json:"flow" db:"flow" cbor:"flow"`                                        //对应的流水
	Amount               string  `json:"amount" db:"amount" cbor:"amount"`                                  //升级奖励金额
	FreeWithdrawNum      int     `json:"free_withdraw_num" db:"free_withdraw_num" cbor:"free_withdraw_num"` //免费提款次数
	WithdrawLimit        int     `json:"withdraw_limit" db:"withdraw_limit" cbor:"withdraw_limit"`          //提款限额
	RebateRate           string  `json:"rebate_rate" db:"rebate_rate" cbor:"rebate_rate"`                   //返水比例
	Props                int     `json:"props" db:"props" cbor:"props"`                                     //道具
	UpdatedAt            int64   `json:"updated_at" db:"updated_at" cbor:"updated_at"`                      //更新时间
	CreatedAt            int64   `json:"created_at" db:"created_at" cbor:"created_at"`                      //创建时间
	WeekAward            float64 `json:"week_award" db:"week_award"`                                        //周奖励金额
	MonthAward           float64 `json:"month_award" db:"month_award"`                                      //月奖励金额
	WithdrawRate         float64 `json:"withdraw_rate" db:"withdraw_rate"`                                  //提现手续费
	WithdrawSingleLimit  int     `json:"withdraw_single_limit" db:"withdraw_single_limit"`                  //提现单笔限额
	ChargeExtraAwardRate float64 `json:"charge_extra_award_rate" db:"charge_extra_award_rate"`              //每笔充值赠送
}

type PromoSignConfig struct {
	Vip             int    `json:"vip" cbor:"vip" db:"vip"`                                           //会员等级
	Sign1Amount     string `json:"sign1_amount" cbor:"sign1_amount" db:"sign1_amount"`                //第一天签到彩金金额
	Sign2Amount     string `json:"sign2_amount" cbor:"sign2_amount" db:"sign2_amount"`                //第二天签到彩金金额
	Sign3Amount     string `json:"sign3_amount" cbor:"sign3_amount" db:"sign3_amount"`                //第三天签到彩金金额
	Sign4Amount     string `json:"sign4_amount" cbor:"sign4_amount" db:"sign4_amount"`                //第四天签到彩金金额
	Sign5Amount     string `json:"sign5_amount" cbor:"sign5_amount" db:"sign5_amount"`                //第五天签到彩金金额
	Sign6Amount     string `json:"sign6_amount" cbor:"sign6_amount" db:"sign6_amount"`                //第六天签到彩金金额
	Sign7Amount     string `json:"sign7_amount" cbor:"sign7_amount" db:"sign7_amount"`                //第七天签到彩金金额
	SignWeekAmount  string `json:"sign_week_amount" cbor:"sign_week_amount" db:"sign_week_amount"`    //周签到彩金金额
	SignMonthAmount string `json:"sign_month_amount" cbor:"sign_month_amount" db:"sign_month_amount"` //月签到彩金金额
}

type PromoSignRecord struct {
	UID       string `json:"uid" cbor:"uid" db:"uid"`
	Username  string `json:"username" cbor:"username" db:"username"`          //会员名
	Vip       int    `json:"vip" cbor:"vip" db:"vip"`                         //签到时的会员等级
	SignWeek1 string `json:"sign_week_1" cbor:"sign_week_1" db:"sign_week_1"` //周第一天签到
	SignWeek2 string `json:"sign_week_2" cbor:"sign_week_2" db:"sign_week_2"` //周第二天签到
	SignWeek3 string `json:"sign_week_3" cbor:"sign_week_3" db:"sign_week_3"` //周第三天签到
	SignWeek4 string `json:"sign_week_4" cbor:"sign_week_4" db:"sign_week_4"` //周第四天签到
	SignWeek5 string `json:"sign_week_5" cbor:"sign_week_5" db:"sign_week_5"` //周第五天签到
	SignWeek6 string `json:"sign_week_6" cbor:"sign_week_6" db:"sign_week_6"` //周第六天签到
	SignWeek7 string `json:"sign_week_7" cbor:"sign_week_7" db:"sign_week_7"` //周第七天签到
	Sign1     string `json:"sign1" cbor:"sign1" db:"sign1"`                   //第一天签到
	Sign2     string `json:"sign2" cbor:"sign2" db:"sign2"`                   //第二天签到
	Sign3     string `json:"sign3" cbor:"sign3" db:"sign3"`                   //第三天签到
	Sign4     string `json:"sign4" cbor:"sign4" db:"sign4"`                   //第四天签到
	Sign5     string `json:"sign5" cbor:"sign5" db:"sign5"`                   //第五天签到
	Sign6     string `json:"sign6" cbor:"sign6" db:"sign6"`                   //第六天签到
	Sign7     string `json:"sign7" cbor:"sign7" db:"sign7"`                   //第七天签到
	Sign8     string `json:"sign8" cbor:"sign8" db:"sign8"`                   //第八天签到
	Sign9     string `json:"sign9" cbor:"sign9" db:"sign9"`                   //第九天签到
	Sign10    string `json:"sign10" cbor:"sign10" db:"sign10"`                //第十天签到
	Sign11    string `json:"sign11" cbor:"sign11" db:"sign11"`                //第十一天签到
	Sign12    string `json:"sign12" cbor:"sign12" db:"sign12"`                //第十二天签到
	Sign13    string `json:"sign13" cbor:"sign13" db:"sign13"`                //第十三天签到
	Sign14    string `json:"sign14" cbor:"sign14" db:"sign14"`                //第十四天签到
	Sign15    string `json:"sign15" cbor:"sign15" db:"sign15"`                //第十五天签到
	Sign16    string `json:"sign16" cbor:"sign16" db:"sign16"`                //第十六天签到
	Sign17    string `json:"sign17" cbor:"sign17" db:"sign17"`                //第十七天签到
	Sign18    string `json:"sign18" cbor:"sign18" db:"sign18"`                //第十八天签到
	Sign19    string `json:"sign19" cbor:"sign19" db:"sign19"`                //第十九天签到
	Sign20    string `json:"sign20" cbor:"sign20" db:"sign20"`                //第二十天签到
	Sign21    string `json:"sign21" cbor:"sign21" db:"sign21"`                //第二十一天签到
	Sign22    string `json:"sign22" cbor:"sign22" db:"sign22"`                //第二十二天签到
	Sign23    string `json:"sign23" cbor:"sign23" db:"sign23"`                //第二十三天签到
	Sign24    string `json:"sign24" cbor:"sign24" db:"sign24"`                //第二十四天签到
	Sign25    string `json:"sign25" cbor:"sign25" db:"sign25"`                //第二十五天签到
	Sign26    string `json:"sign26" cbor:"sign26" db:"sign26"`                //第二十六天签到
	Sign27    string `json:"sign27" cbor:"sign27" db:"sign27"`                //第二十七天签到
	Sign28    string `json:"sign28" cbor:"sign28" db:"sign28"`                //第二十八天签到
	Sign29    string `json:"sign29" cbor:"sign29" db:"sign29"`                //第二十九天签到
	Sign30    string `json:"sign30" cbor:"sign30" db:"sign30"`                //第三十天签到
	LastSign  string `json:"last_sign" cbor:"last_sign" db:"last_sign"`       //最后一次签到时间yyyy-mm-dd
}

type PromoSignRewardRecord struct {
	ID        string `json:"id" db:"id" cbor:"id"`                         //id
	UID       string `json:"uid" db:"uid" cbor:"uid"`                      //uid
	Username  string `json:"username" db:"username" cbor:"username"`       //会员名
	Vip       int    `json:"vip" db:"vip" cbor:"vip"`                      //vip
	Day       int    `json:"day" db:"day" cbor:"day"`                      //第几天
	MonthDay  int    `json:"month_day" db:"month_day" cbor:"month_day"`    //月签到第几天
	Amount    string `json:"amount" db:"amount" cbor:"amount"`             //奖金金额
	CreatedAt int64  `json:"created_at" db:"created_at" cbor:"created_at"` //记录时间
}

// 账变表
type MemberTransaction struct {
	AfterAmount  string `db:"after_amount" json:"after_amount" cbor:"after_amount"`    //账变后的金额
	Amount       string `db:"amount" json:"amount" cbor:"amount"`                      //用户填写的转换金额
	BeforeAmount string `db:"before_amount" json:"before_amount" cbor:"before_amount"` //账变前的金额
	BillNo       string `db:"bill_no" json:"bill_no" cbor:"bill_no"`                   //转账|充值|提现ID
	CashType     int    `db:"cash_type" json:"cash_type" cbor:"cash_type"`             //0:转入1:转出2:转入失败补回3:转出失败扣除4:存款5:提现
	CreatedAt    int64  `db:"created_at" json:"created_at" cbor:"created_at"`          //
	ID           string `db:"id" json:"id" cbor:"id"`                                  //
	UID          string `db:"uid" json:"uid" cbor:"uid"`                               //用户ID
	Username     string `db:"username" json:"username" cbor:"username"`                //用户名
	Remark       string `db:"remark" json:"remark" cbor:"remark"`                      //备注
}

type PromoTreasureConfig struct {
	ID          string `json:"id" cbor:"id" db:"id"`
	InviteNum   uint32 `json:"invite_num" cbor:"invite_num" db:"invite_num"`       //邀请人数
	Amount      string `json:"amount" cbor:"amount" db:"amount"`                   //宝箱金额
	TotalAmount string `json:"total_amount" cbor:"total_amount" db:"total_amount"` //累计宝箱金额
}

type PromoTreasureRecord struct {
	ID        string `json:"id" cbor:"id" db:"id"`
	UID       string `json:"uid" cbor:"uid" db:"uid"`
	Username  string `json:"username" cbor:"username" db:"username"`       //会员名
	InviteNum int    `json:"invite_num" cbor:"invite_num" db:"invite_num"` //邀请人数
	Amount    string `json:"amount" cbor:"amount" db:"amount"`             //宝箱金额
	CreatedAt int64  `json:"created_at" cbor:"created_at" db:"created_at"` //记录时间
}

// 游戏列表返回数据结构
type GameData struct {
	D []Game `json:"d" cbor:"d"`
	T int64  `json:"t" cbor:"t"`
	S uint   `json:"s" cbor:"s"`
}

// 数据库 游戏字段
type Game struct {
	ID         string `json:"id" db:"id" cbor:"id"`
	PlatformID string `json:"platform_id" db:"platform_id" cbor:"platform_id"` //场馆ID
	Name       string `json:"name" db:"name" cbor:"name"`                      //游戏名称
	EnName     string `json:"en_name" db:"en_name" cbor:"en_name"`             //英文名称
	BrAlias    string `json:"br_alias" db:"br_alias" cbor:"br_alias"`          //巴西别名
	ClientType string `json:"client_type" db:"client_type" cbor:"client_type"` //0:all 1:web 2:h5 4:app 此处值为支持端的数值之和
	GameType   int    `json:"game_type" db:"game_type" cbor:"game_type"`       //游戏类型:1=真人,2=捕鱼,3=电子,4=体育
	GameID     string `json:"game_id" db:"game_id" cbor:"game_id"`             //游戏ID
	Img        string `json:"img" db:"img" cbor:"img"`                         //手机图片
	Online     int    `json:"online" db:"online" cbor:"online"`                //0 下线 1上线
	IsHot      int    `json:"is_hot" db:"is_hot" cbor:"is_hot"`                //0 正常 1热门
	IsFav      int    `json:"is_fav" db:"is_fav" cbor:"is_fav"`                //0 正常 1热门
	IsNew      int    `json:"is_new" db:"is_new" cbor:"is_new"`                //是否最新:0=否,1=是
	Sorting    int    `json:"sorting" db:"sorting" cbor:"sorting"`             //排序
	CreatedAt  int64  `json:"created_at" db:"created_at" cbor:"created_at"`    //添加时间
}

type Email struct {
	URL      string `toml:"url"`
	Port     string `toml:"port"`
	Username string `toml:"username"`
	Password string `toml:"password"`
}

type AwsEmail struct {
	AccessKeyID     string `toml:"accessKeyID"`
	SecretAccessKey string `toml:"secretAccessKey"`
	Region          string `toml:"region"`
	From            string `toml:"from"`
}

type TgPay struct {
	AppKey string `toml:"appKey"`
}

type ApplyParam struct {
	Uid      string  `json:"uid"`
	Username string  `json:"username"`
	Amount   float64 `json:"amount"`
}

package model

import (
	"errors"
	"fmt"
	"member/contrib/helper"
	"time"

	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
)

func smsBuka(phone, code string) error {

	uri := fmt.Sprintf("%s?appId=%s&numbers=%s&senderId=%s&content=%s", smsConf["buka"]["api"], smsConf["buka"]["appid"], phone, "", code)
	fmt.Println(uri)
	ts := time.Now().Unix()
	sign := helper.MD5Hash(fmt.Sprintf(`%s%s%d`, smsConf["buka"]["key"], smsConf["buka"]["secret"], ts))
	header := map[string]string{
		"Content-Type": "application/json",
		"Sign":         sign,
		"Timestamp":    fmt.Sprintf(`%d`, ts),
		"Api-Key":      smsConf["buka"]["key"],
	}
	body, statusCode, err := httpDoTimeout(nil, "GET", uri, header, time.Duration(10)*time.Second)
	if err != nil {
		fmt.Println("err = ", err.Error())
		//return errors.New(helper.RequestFail)
	}
	fmt.Println("statusCode:", statusCode)
	fmt.Println("return:", string(body))

	if statusCode != fasthttp.StatusOK {
		return errors.New(helper.RequestFail)
	}

	var p fastjson.Parser

	v, err := p.ParseBytes(body)
	if string(v.GetStringBytes("status")) == "0" {
		return nil
	}

	fmt.Println("MOBILE SMS OUT")
	return errors.New(helper.VerificationSendErr)
}

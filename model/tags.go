package model

import (
	"fmt"

	"github.com/meilisearch/meilisearch-go"
)

type Tag_t struct {
	Tid        string `db:"tid" json:"tid"`
	Name       string `db:"name" json:"name"`
	State      string `db:"state" json:"state"` //0:关闭1:开启
	CreatedAt  uint32 `db:"created_at" json:"created_at"`
	GameType   int    `db:"game_type" json:"game_type"`     //场馆类型
	PlatformId int    `db:"platform_id" json:"platform_id"` //游戏ID
}

func TagList(platformId, gameType string) ([]Tag_t, error) {

	var data []Tag_t

	index := meta.Meili.Index("tags")

	cond := &meilisearch.SearchRequest{
		Limit: 1000,
	}

	filter := ""
	if gameType != "0" {
		filter = fmt.Sprintf("game_type = %s", gameType)
	}

	if platformId != "0" {
		if len(filter) == 0 {
			filter = fmt.Sprintf("platformId = %s", platformId)
		} else {
			filter += fmt.Sprintf(" AND platformId = %s", platformId)
		}

	}

	cond.Filter = filter
	searchRes, err := index.Search("", cond)
	if err != nil {
		return data, err
	}

	ll := len(searchRes.Hits)
	if ll == 0 {
		return data, err
	}

	data = make([]Tag_t, ll)

	for i, v := range searchRes.Hits {
		val := v.(map[string]interface{})

		data[i].Tid = val["tid"].(string)
		data[i].Name = val["name"].(string)
		data[i].State = val["state"].(string)
		data[i].GameType = int(val["game_type"].(float64))
		data[i].PlatformId = int(val["platform_id"].(float64))
	}

	return data, nil
}

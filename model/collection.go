package model

import (
	"common/config"
	myUserHelp "common/userHelp"
	"errors"
	"fmt"
	"member/contrib/helper"
	"member/contrib/validator"
	"time"

	"github.com/shopspring/decimal"

	"github.com/valyala/fasthttp"
)

func Recharge(fctx *fasthttp.RequestCtx, prefix, fid, amount, flag string) (PaymentDepositResp, error) {

	res := PaymentDepositResp{}

	uid := GetUidFromToken(fctx)
	if uid == "" {
		return res, errors.New(helper.AccessTokenExpires)
	}

	user, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		return res, errors.New(helper.UserNotExist)
	}

	if user.State == 4 {
		return res, errors.New(helper.Blocked)
	}

	fmt.Printf("deposit username[%s] args: %s, ts: %s  \n",
		user.Username, fctx.PostArgs().String(), time.Now().Format("2006-01-02 15:04:05"))

	p, err := CachePayFactory(fid)
	if err != nil {
		return res, errors.New(helper.ChannelNotExist)
	}

	res, err = Pay(fctx, prefix, user, p, amount, flag)
	if err != nil {
		return res, err
	}

	return res, nil
}

// Pay 发起支付公共入口
func Pay(fctx *fasthttp.RequestCtx, prefix string, user myUserHelp.TblMemberBase, p TblPayFactory, amount, flag string) (PaymentDepositResp, error) {

	data := PaymentDepositResp{}

	//payment, ok := thirdFuncCb[p.Fid]
	//if !ok {
	//	return data, errors.New(helper.NoPayChannel)
	//}

	// 检查存款金额是否符合范围
	a, ok := validator.CheckFloatScope(amount, p.Fmin, p.Fmax)
	if !ok {
		return data, errors.New(helper.AmountOutRange)
	}

	amount = a.String()
	// 生成我方存款订单号
	orderId := helper.GenId()
	//计算优惠
	var discount, bonusAmount decimal.Decimal
	if flag == "1" {
		var PromoDepositType int
		if user.DepositAmount == 0 {
			PromoDepositType = config.PromoDeposit_First
		} else {
			PromoDepositType = config.PromoDeposit_Common
		}

		pdc := config.PromoDepositConfigList(PromoDepositType)

		for _, v := range pdc {
			if a.GreaterThanOrEqual(decimal.NewFromFloat(v.MinAmount)) {
				discount = decimal.NewFromFloat(v.Bonus).Div(decimal.NewFromInt(100))
			}
			//计算多少赠送礼金
			bonusAmount = a.Mul(discount)
			if v.MaxAmount > 0 {
				if bonusAmount.GreaterThanOrEqual(decimal.NewFromFloat(v.MaxAmount)) {
					bonusAmount = decimal.NewFromFloat(v.MaxAmount)
				}
			}
			break
		}

	}
	//计算多少赠送礼金
	//bonusAmount := a.Mul(discount)
	fmt.Println("Pay orderId = ", orderId)
	// 向渠道方发送存款订单请求
	payment := &thirdEpay{}
	data, err := payment.CreateEpayOrder(amount, orderId, user.Uid, p)
	//data, err := payment.Deposit(fctx, user.Username, amount, orderId, p)
	fmt.Println("Pay  payment.Pay err = ", err)
	if err != nil {
		return data, err
	}

	// 生成我方存款订单号
	d := tblDeposit{
		Id:         data.OrderID,
		Oid:        data.Oid,
		Uid:        user.Uid,
		TopId:      user.TopID,
		TopName:    user.TopName,
		ParentName: user.ParentName,
		ParentId:   user.ParentID,
		Username:   user.Username,
		Fid:        p.Fid,
		Fname:      p.Name,
		Amount:     amount,
		State:      DepositConfirming,
		CreatedAt:  fctx.Time().Unix(),
		Tester:     user.Tester,
		Discount:   bonusAmount.InexactFloat64(),
		Prefix:     prefix,
	}

	if p.Ty == 2 {
		d.UsdtRate = p.PayRate
		d.UsdtCount = data.UsdtCount
	}

	fmt.Println("deposit d:", d)
	// 请求成功插入订单
	err = insertDeposit(d)
	if err != nil {
		fmt.Println("insert into table error: = ", err)
		return data, errors.New(helper.DBErr)
	}

	return data, nil
}

package model

import (
	"errors"
	"fmt"
	"member/contrib/helper"
	"time"

	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
)

func smsKmi(phone, code string) error {

	header := map[string]string{
		"Content-Type": "application/json",
	}
	recs := map[string]interface{}{
		"accessKey":   smsConf["kmi"]["key"],
		"secretKey":   smsConf["kmi"]["secret"],
		"to":          phone,
		"message":     smsConf["kmi"]["text"] + code,
		"callbackUrl": smsConf["kmi"]["callbackUrl"],
	}
	requestBody, err := helper.JsonMarshal(recs)
	body, statusCode, err := httpDoTimeout(requestBody, "POST", smsConf["kmi"]["api"], header, time.Duration(10)*time.Second)
	if err != nil {
		fmt.Println("err = ", err.Error())
	}
	fmt.Println("statusCode:", statusCode)
	fmt.Println("return:", string(body))

	if statusCode != fasthttp.StatusOK {
		return errors.New(helper.RequestFail)
	}

	var p fastjson.Parser

	v, err := p.ParseBytes(body)
	if string(v.GetStringBytes("code")) == "200" {
		return nil
	}

	fmt.Println("MOBILE SMS OUT")
	return errors.New(helper.VerificationSendErr)
}

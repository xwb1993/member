package model

import (
	myConfig "common/config"
	myredis "common/redis"
	myUserHelp "common/userHelp"
	"database/sql"
	"errors"
	"fmt"
	"member/contrib/helper"
	"member/contrib/validator"
	"strconv"
	"time"

	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
)

// tblWithdraw 出款
type tblWithdraw struct {
	ID             string  `db:"id" json:"id" cbor:"id"`                                        //
	OID            string  `db:"oid" json:"oid" cbor:"oid"`                                     //三方订单
	UID            string  `db:"uid" json:"uid" cbor:"uid"`                                     //用户ID
	Username       string  `db:"username" json:"username" cbor:"username"`                      //用户名
	TopId          string  `db:"top_id" json:"top_id" cbor:"top_id"`                            //总代uid
	TopName        string  `db:"top_name" json:"top_name" cbor:"top_name"`                      //总代代理
	ParentId       string  `db:"parent_id" json:"parent_id" cbor:"parent_id"`                   //上级uid
	ParentName     string  `db:"parent_name" json:"parent_name" cbor:"parent_name"`             //上级代理
	FID            string  `db:"fid" json:"fid" cbor:"fid"`                                     //通道id
	Fname          string  `db:"fname" json:"fname" cbor:"fname"`                               //通道名称
	Amount         float64 `db:"amount" json:"amount" cbor:"amount"`                            //金额
	Fee            float64 `db:"fee" json:"fee" cbor:"fee"`                                     //手续费
	State          int     `db:"state" json:"state" cbor:"state"`                               //0:待确认:1存款成功2:已取消
	Automatic      int     `db:"automatic" json:"automatic" cbor:"automatic"`                   //1:自动转账2:脚本确认3:人工确认
	PixAccount     string  `db:"pix_account" json:"pix_account" cbor:"pix_account"`             //银行名
	RealName       string  `db:"real_name" json:"real_name" cbor:"real_name"`                   //持卡人姓名
	PixId          string  `db:"pix_id" json:"pix_id" cbor:"pix_id"`                            //银行卡号
	CreatedAt      int64   `db:"created_at" json:"created_at" cbor:"created_at"`                //
	ConfirmAt      int64   `db:"confirm_at" json:"confirm_at" cbor:"confirm_at"`                //确认时间
	ConfirmUID     string  `db:"confirm_uid" json:"confirm_uid" cbor:"confirm_uid"`             //确认人uid
	ConfirmName    string  `db:"confirm_name" json:"confirm_name" cbor:"confirm_name"`          //确认人名
	ReviewRemark   string  `db:"review_remark" json:"review_remark" cbor:"review_remark"`       //确认人名
	WithdrawAt     int64   `db:"withdraw_at" json:"withdraw_at" cbor:"withdraw_at"`             //三方场馆ID
	WithdrawRemark string  `db:"withdraw_remark" json:"withdraw_remark" cbor:"withdraw_remark"` //确认人名
	WithdrawUID    string  `db:"withdraw_uid" json:"withdraw_uid" cbor:"withdraw_uid"`          //确认人uid
	WithdrawName   string  `db:"withdraw_name" json:"withdraw_name" cbor:"withdraw_name"`       //确认人名
	HangUpUID      string  `db:"hang_up_uid" json:"hang_up_uid" cbor:"hang_up_uid"`             // 挂起人uid
	HangUpRemark   string  `db:"hang_up_remark" json:"hang_up_remark" cbor:"hang_up_remark"`    // 挂起备注
	HangUpName     string  `db:"hang_up_name" json:"hang_up_name" cbor:"hang_up_name"`          //  挂起人名字
	RemarkID       int     `db:"remark_id" json:"remark_id" cbor:"remark_id"`                   // 挂起原因ID
	HangUpAt       int     `db:"hang_up_at" json:"hang_up_at" cbor:"hang_up_at"`                //  挂起时间
	ReceiveAt      int64   `db:"receive_at" json:"receive_at" cbor:"receive_at"`                //领取时间
	Balance        string  `db:"balance" json:"balance" cbor:"balance"`                         //提现前的余额
	Flag           string  `json:"flag" db:"flag"  cbor:"flag"`                                 //1 cpf 2 phone 3 email

}

// paymentWithdrawResp 提款
type paymentWithdrawResp struct {
	OrderID string `json:"order_id" cbor:"order_id"` //我方订单号
	Oid     string `json:"oid" cbor:"oid"`           //三方的订单号
}

// 订单回调response
type paymentCallbackResp struct {
	AppId           string `json:"appId"`           //机构号
	CustId          string `json:"custId"`          //商户编号
	MerchantOrderId string `json:"merchantOrderId"` // 商户订单号
	Order           string `json:"order"`           // 平台订单号
	OrderStatus     string `json:"orderStatus"`     // 业务状态
	Amount          string `json:"amount"`          //金额
	Sign            string `json:"sign"`            // 签名
}

type TblRatelist struct {
	Id          int     `json:"id"`
	Ty          uint32  `json:"ty"`           //类型 0充值 1下发
	Rate        float64 `json:"rate"`         //手续费率 %
	Min         float64 `json:"min"`          //最少收取
	Max         float64 `json:"max"`          //最高收取
	UpdatedAt   uint32  `json:"updated_at"`   //修改时间
	UpdatedName string  `json:"updated_name"` //修改人
}

func WithdrawApply(fctx *fasthttp.RequestCtx, prefix, amount, bankId, payPassword, fid string) (string, error) {

	uid := GetUidFromToken(fctx)
	if uid == "" {
		return "", errors.New(helper.AccessTokenExpires)
	}

	member, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		return "", errors.New(helper.UserNotExist)
	}

	if member.State == 4 {
		return "", errors.New(helper.Blocked)
	}

	bank, err := BankcardFindOne(bankId)
	if err != nil {
		return "", errors.New(helper.BankcardIDErr)
	}
	if bank.Uid != member.Uid {
		return "", errors.New(helper.BankcardIDErr)
	}

	if member.PayPassword == "0" || member.PayPassword == "" {
		return "", errors.New(helper.SetWithdrawPwdFirst)
	}

	if payPassword == "" {
		return "", errors.New(helper.WithdrawPwdMismatch)
	}

	//pwd := fmt.Sprintf("%d", MurmurHash(payPassword, member.CreatedAt))
	//if pwd != member.PayPassword {
	if payPassword != member.PayPassword {
		return "", errors.New(helper.WithdrawPwdMismatch)
	}

	memberVip, err := getVip(member.Vip)
	if err != nil {
		return "", err
	}

	todayCount, todayAmount, err := withdrawToday(member.Uid)
	if err != nil {
		return "", err
	}
	if member.Tester != 2 && todayCount >= int64(memberVip.FreeWithdrawNum) {
		return "", errors.New(helper.DailyTimesLimitErr)
	}
	amountInt, err := strconv.Atoi(amount)
	if err != nil {
		return "", err
	}
	if member.Tester != 2 && amountInt >= memberVip.WithdrawSingleLimit {
		return "", errors.New(helper.AmountOutRange)
	}

	if member.Tester != 2 && todayAmount >= int64(memberVip.WithdrawLimit) {
		return "", errors.New(helper.DailyAmountLimitErr)
	}

	withdrawId := helper.GenLongId()
	fmt.Println("withdrawId:", withdrawId)
	lk := fmt.Sprintf("w:%s", member.Username)
	err = withLock(lk)
	if err != nil {
		return "", err
	}

	defer Unlock(lk)
	// 同时只能有一笔提款在处理中
	ex := g.Ex{
		"uid":   member.Uid,
		"state": g.Op{"notIn": []int64{WithdrawReviewReject, WithdrawSuccess, WithdrawFailed}},
	}

	err = withdrawOrderExists(ex)
	if err != nil {
		fmt.Println(err)
		return "", err
	}

	withdrawAmount, err := decimal.NewFromString(amount)
	if err != nil {
		fmt.Println(err)
		return "", pushLog(err, helper.AmountErr)
	}
	rate := decimal.NewFromFloat(memberVip.WithdrawRate).Div(decimal.NewFromInt(100))
	feeReckon, _ := rate.Mul(withdrawAmount).Float64()
	fee := fmt.Sprintf("%.2f", feeReckon)
	feeAmount, _ := decimal.NewFromString(fee)

	// check balance
	userAmount, err := BalanceIsEnough(member.Uid, withdrawAmount.Add(feeAmount))
	if err != nil {
		fmt.Println(err)
		return "", err
	}

	record := g.Record{
		"id":           withdrawId,
		"oid":          withdrawId,
		"uid":          member.Uid,
		"top_id":       member.TopID,
		"top_name":     member.TopName,
		"parent_name":  member.ParentName,
		"parent_id":    member.ParentID,
		"username":     member.Username,
		"fid":          fid,
		"amount":       withdrawAmount.Truncate(4).String(),
		"fee":          feeAmount,
		"state":        WithdrawReviewing,
		"automatic":    0, //1:自动转账  0:人工确认
		"created_at":   fctx.Time().Unix(),
		"real_name":    bank.RealName,
		"pix_id":       bank.PixId,
		"pix_account":  bank.PixAccount,
		"receive_at":   0,
		"confirm_uid":  0,
		"confirm_name": "",
		"level":        member.Vip,
		"tester":       member.Tester,
		"balance":      userAmount.Sub(withdrawAmount.Add(feeAmount)).String(),
		"fname":        "Withdraw",
		"flag":         bank.Flag,
		"prefix":       prefix,
	}
	if member.Tester == 2 {
		record["state"] = WithdrawSuccess
		record["withdraw_at"] = time.Now().Unix() + 1
	}

	if err != nil {
		fmt.Println(err)
		return "", pushLog(err, helper.DBErr)
	}

	query, _, _ := dialect.Insert("tbl_withdraw").Rows(record).ToSQL()
	myredis.UpdateSqlFieldToRedis(query)

	// 更新余额
	ex = g.Ex{
		"uid": member.Uid,
	}

	if meta.WalletMode == "1" {
		myredis.AddUserFieldValueByDemical(member.Uid, myConfig.G_tbl_member_balance+"brl", withdrawAmount.Add(feeAmount))
		myredis.AddUserFieldValueByDemical(member.Uid, myConfig.G_tbl_member_balance+"lock_amount", withdrawAmount.Add(feeAmount))

	} else if meta.WalletMode == "2" {
		myredis.AddUserFieldValueByDemical(member.Uid, myConfig.G_tbl_member_balance+"brl", withdrawAmount.Add(feeAmount))
		myredis.AddUserFieldValueByDemical(member.Uid, myConfig.G_tbl_member_balance+"lock_amount", withdrawAmount.Add(feeAmount))
		myredis.AddUserFieldValueByDemical(member.Uid, myConfig.G_tbl_member_balance+"unlock_amount", withdrawAmount.Add(feeAmount))
	}

	beforeAmount := userAmount
	afterAmount := beforeAmount.Sub(withdrawAmount)
	// 写入账变
	mbTrans := MemberTransaction{
		AfterAmount:  afterAmount.String(),
		Amount:       withdrawAmount.String(),
		BeforeAmount: beforeAmount.String(),
		BillNo:       withdrawId,
		CreatedAt:    time.Now().UnixMilli(),
		ID:           helper.GenId(),
		CashType:     helper.TransactionWithDraw,
		UID:          member.Uid,
		Username:     member.Username,
		Remark:       "withdraw",
	}
	query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(mbTrans).ToSQL()
	myredis.AddLogToRedis(query)

	beforeAmount = afterAmount
	afterAmount = beforeAmount.Sub(feeAmount)
	// 写入账变手续费
	mbTransFee := MemberTransaction{
		AfterAmount:  afterAmount.String(),
		Amount:       feeAmount.String(),
		BeforeAmount: beforeAmount.String(),
		BillNo:       withdrawId,
		CreatedAt:    time.Now().UnixMilli(),
		ID:           helper.GenId(),
		CashType:     helper.TransactionWithdrawalFee,
		UID:          member.Uid,
		Username:     member.Username,
		Remark:       "withdraw fee",
	}

	query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(mbTransFee).ToSQL()
	myredis.AddLogToRedis(query)
	return withdrawId, nil
}

// WithdrawHandToAuto
func withdrawHandToAuto(fctx *fasthttp.RequestCtx, p TblPayFactory, uid, username, id string, amount float64, withdraw tblWithdraw) error {

	if len(p.Fid) == 0 {
		return errors.New(helper.CateNotExist)
	}

	//user, err := GetMemberBaseInfo(fctx, username)
	//if err != nil {
	//	return err
	//}

	as := strconv.FormatFloat(amount, 'f', -1, 64)
	// check amount range, continue the for loop if amount out of range
	_, ok := validator.CheckFloatScope(as, p.Fmin, p.Fmax)
	if !ok {
		return errors.New(helper.AmountOutRange)
	}
	//payment, ok := thirdFuncCb[p.Fid]
	//if !ok {
	//	return errors.New(helper.NoPayChannel)
	//}
	payment := &thirdEpay{}
	//data, err := payment.Withdraw(fctx, time.Now(), user.Uid, as, id, withdraw.PixAccount, withdraw.RealName, withdraw.PixId, withdraw.Flag, p)
	err := payment.Withdraw(as, id, withdraw.PixAccount, uid, withdraw.PixId, withdraw.Flag, p)
	fmt.Println("Pay  payment.Pay err = ", err)
	if err != nil {
		return err
	}
	err = withdrawAutoUpdate(id, WithdrawAutoPaying)
	if err != nil {
		fmt.Println("withdrawHandToAuto failed 2:", id, err)
	}
	return nil
}

func withdrawAutoUpdate(id string, state int) error {

	r := g.Record{"state": state, "automatic": "1"}
	query, _, _ := dialect.Update("tbl_withdraw").Set(r).Where(g.Ex{"id": id}).ToSQL()
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	return err
}

func withLock(id string) error {

	val := fmt.Sprintf("%s:%s", defaultRedisKeyPrefix, id)
	ok, err := meta.MerchantRedis.SetNX(ctx, val, "1", 120*time.Second).Result()
	if err != nil {
		return pushLog(err, helper.RedisErr)
	}
	if !ok {
		return errors.New(helper.RequestBusy)
	}

	return nil
}

// 检查订单是否存在
func withdrawOrderExists(ex g.Ex) error {

	var id string
	query, _, _ := dialect.From("tbl_withdraw").Select("id").Where(ex).ToSQL()
	err := meta.MerchantDB.Get(&id, query)

	if err != nil && err != sql.ErrNoRows {
		return pushLog(err, helper.DBErr)
	}

	if id != "" {
		return errors.New(helper.OrderProcess)
	}

	return nil
}

type withdrawTodayData struct {
	Total  sql.NullInt64 `db:"total"`
	Amount sql.NullInt64 `db:"amount"`
}

// 查今天提款次数和金额
func withdrawToday(uid string) (int64, int64, error) {

	var data withdrawTodayData
	ex := g.Ex{
		"uid":        uid,
		"state":      WithdrawSuccess,
		"created_at": helper.DayTST(0, loc).Unix(),
	}

	query, _, _ := dialect.From("tbl_withdraw").Select(g.COUNT("id").As("total"), g.SUM("amount").As("amount")).Where(ex).GroupBy(g.C("uid")).ToSQL()
	err := meta.MerchantDB.Get(&data, query)

	if err != nil && err != sql.ErrNoRows {
		return 0, 0, pushLog(err, helper.DBErr)
	}

	return data.Total.Int64, data.Amount.Int64, nil
}

func BankCardExist(ex g.Ex) bool {

	var id string
	t := dialect.From("tbl_member_bankcard")
	query, _, _ := t.Select("uid").Where(ex).Limit(1).ToSQL()
	fmt.Println("BankCardExist:", query)
	err := meta.MerchantDB.Get(&id, query)
	if err == sql.ErrNoRows {
		return false
	}

	return true
}

// BalanceIsEnough 检查中心钱包余额是否充足
func BalanceIsEnough(uid string, amount decimal.Decimal) (decimal.Decimal, error) {

	balance, err := myUserHelp.GetMemberBalanceInfo(uid)
	if !err {
		return decimal.NewFromFloat(balance.Brl), errors.New(helper.UserNotExist)
	}
	brl := decimal.NewFromFloat(balance.Brl)
	//ud := decimal.NewFromFloat(balance.DepositLockAmount)
	//al := decimal.NewFromFloat(balance.AgencyLockAmount)
	unLockAmount := decimal.NewFromFloat(balance.UnlockAmount)
	fmt.Println("brl===", brl)
	if unLockAmount.Sub(amount).IsNegative() {
		return brl, errors.New(helper.LackOfBalance)
	}
	return brl, nil
}

func WithdrawConfig(fctx *fasthttp.RequestCtx) (WithdrawConfigData, error) {

	data := WithdrawConfigData{}
	uid := GetUidFromToken(fctx)
	if uid == "" {
		return data, errors.New(helper.AccessTokenExpires)
	}

	member, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		return data, errors.New(helper.UserNotExist)
	}

	channels, err := ChannelList(fctx, 2)
	if err != nil || channels.T == 0 {
		fmt.Println("ChannelList err: ", err)

		return data, errors.New(helper.NoPayChannel)
	}
	data.Config = channels.D[0]
	bls, err := BankcardList(member.Uid)
	if err != nil {
		fmt.Println("BankcardList err: ", err)
		return data, nil
	}
	data.MemberBankList = bls.D
	data.MemberBankT = bls.T
	memberVip, _ := getVip(member.Vip)
	data.WithdrawLimit = memberVip.WithdrawLimit
	data.FreeWithdrawNum = memberVip.FreeWithdrawNum
	return data, nil
}

// WithdrawalCallBack 提款回调
func WithdrawalCallBack(fctx *fasthttp.RequestCtx) {
	var (
		err  error
		data paymentCallbackResp
	)
	payment := &thirdEpay{}
	// 获取并校验回调参数
	data, err = payment.WithdrawCallBack(fctx)
	if err != nil {
		fctx.SetBody([]byte(`failed`))
		pushLog(err, helper.WithdrawFailure)
		return
	}
	fmt.Println("获取并校验回调参数:", data)

	// 查询订单
	order, err := withdrawFind(data.MerchantOrderId)
	if err != nil {
		err = fmt.Errorf("query order error: [%v]", err)
		fctx.SetBody([]byte(`failed`))
		pushLog(err, helper.WithdrawFailure)
		return
	}

	//pLog.Username = order.Username
	//pLog.OrderID = data.OrderID0

	// 提款成功只考虑出款中和代付失败的情况
	// 审核中的状态不用考虑，因为不会走到三方去，出款成功和出款失败是终态也不用考虑
	if order.State != WithdrawAutoPaying {
		err = fmt.Errorf("duplicated Withdrawal notify: [%v]", err)
		fctx.SetBody([]byte(`failed`))
		pushLog(err, helper.WithdrawFailure)
		return
	}

	now := fctx.Time()
	// 修改订单状态
	var state int
	if data.OrderStatus != "07" {
		state = WithdrawAutoPayFailed
	} else {
		state = WithdrawSuccess
	}
	err = withdrawUpdate(data.MerchantOrderId, order.UID, "", state, now)
	if err != nil {
		err = fmt.Errorf("set order state [%d] to [%d] error: [%v]", order.State, state, err)
		pushLog(err, helper.WithdrawFailure)
		fctx.SetBody([]byte(`failed`))
		return
	}
	fctx.SetBody([]byte(`success`))
}

// 查找单条提款记录, 订单不存在返回错误: OrderNotExist
func withdrawFind(id string) (tblWithdraw, error) {

	w := tblWithdraw{}
	query, _, _ := dialect.From("tbl_withdraw").Select(colsWithdraw...).Where(g.Ex{"id": id}).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&w, query)
	fmt.Println(query)
	if err == sql.ErrNoRows {
		return w, errors.New(helper.OrderNotExist)
	}

	if err != nil {
		return w, err
	}

	return w, nil
}

// 接收到三方回调后调用这个方法（三方调用缺少confirm uid和confirm name这些信息）
func withdrawUpdate(id, uid, bid string, state int, t time.Time) error {

	// 加锁
	err := withdrawLock(id)
	if err != nil {
		return err
	}
	defer withdrawUnLock(id)

	record := g.Record{
		"state": state,
	}

	switch state {
	case WithdrawSuccess:
		record["automatic"] = "1"
		record["withdraw_at"] = fmt.Sprintf("%d", t.Unix())
	case WithdrawAutoPayFailed:
		record["confirm_at"] = fmt.Sprintf("%d", t.Unix())
	default:
		return errors.New(helper.StateParamErr)
	}

	return withdrawDownPoint(id, "", state, record)
}

// WithdrawLock 锁定提款订单
// 订单因为外部因素(接口)导致的状态流转应该加锁
func withdrawLock(id string) error {

	key := fmt.Sprintf(withdrawOrderLockKey, id)
	err := Lock(key)
	if err != nil {
		return err
	}

	return nil
}

// WithdrawUnLock 解锁提款订单
func withdrawUnLock(id string) {
	Unlock(fmt.Sprintf(withdrawOrderLockKey, id))
}

// 取款下分
func withdrawDownPoint(did, bankcard string, state int, record g.Record) error {

	//判断状态是否合法
	allow := map[int]bool{
		WithdrawReviewReject:  true,
		WithdrawDealing:       true,
		WithdrawSuccess:       true,
		WithdrawFailed:        true,
		WithdrawAutoPayFailed: true,
	}
	if _, ok := allow[state]; !ok {
		return errors.New(helper.StateParamErr)
	}

	//1、判断订单是否存在
	var order tblWithdraw
	ex := g.Ex{"id": did}
	query, _, _ := dialect.From("tbl_withdraw").Select(colsWithdraw...).Where(ex).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&order, query)
	if err != nil || len(order.Username) < 1 {
		return errors.New(helper.IDErr)
	}

	query, _, _ = dialect.Update("tbl_withdraw").Set(record).Where(ex).ToSQL()
	fmt.Println(query)
	switch order.State {
	case WithdrawReviewing:
		// 审核中(风控待领取)的订单只能流向分配, 上层业务处理
		return errors.New(helper.OrderStateErr)
	case WithdrawDealing, WithdrawAutoPaying:
		// 出款处理中可以是自动代付失败(WithdrawAutoPayFailed) 提款成功(WithdrawSuccess) 提款失败(WithdrawFailed)
		// 自动代付失败和提款成功是调用三方代付才会有的状态
		// 提款失败通过提款管理的[拒绝]操作进行流转(同时出款类型必须是手动出款)
		if state == WithdrawAutoPayFailed {
			_, err = meta.MerchantDB.Exec(query)
			if err != nil {
				return pushLog(err, helper.DBErr)
			}

			return nil
		}

		if state != WithdrawSuccess && (state != WithdrawFailed) {
			return errors.New(helper.OrderStateErr)
		}

	case WithdrawAutoPayFailed:
		// 代付失败可以通过手动代付将状态流转至出款中
		if state == WithdrawDealing {
			_, err = meta.MerchantDB.Exec(query)
			if err != nil {
				return pushLog(err, helper.DBErr)
			}

			return nil
		}

		// 代付失败的订单也可以通过手动出款直接将状态流转至出款成功
		// 代付失败的订单还可以通过拒绝直接将状态流转至出款失败
		if state != WithdrawFailed && state != WithdrawSuccess {
			return errors.New(helper.OrderStateErr)
		}

	case WithdrawHangup:
		// 挂起的订单只能领取(该状态流转上传业务已经处理), 该状态只能流转至审核中(WithdrawReviewing)
		return errors.New(helper.OrderStateErr)

	case WithdrawDispatched:
		// 派单状态可流转状态为 挂起(WithdrawHangup) 通过(WithdrawDealing) 拒绝(WithdrawReviewReject)
		// 其中流转至挂起状态由上层业务处理
		if state == WithdrawDealing {
			_, err = meta.MerchantDB.Exec(query)
			if err != nil {
				return pushLog(err, helper.DBErr)
			}

			return nil
		}

		if state != WithdrawReviewReject {
			return errors.New(helper.OrderStateErr)
		}

	default:
		// 审核拒绝, 提款成功, 出款失败三个状态为终态 不能进行其他处理
		return errors.New(helper.OrderStateErr)
	}

	// 3 如果是出款成功,修改订单状态为提款成功,扣除锁定钱包中的钱,发送通知
	if WithdrawSuccess == state {
		return withdrawOrderSuccess(query, order)
	}

	order.ReviewRemark = record["review_remark"].(string)
	order.WithdrawRemark = record["withdraw_remark"].(string)
	// 出款失败
	return withdrawOrderFailed(query, order)
}

// 提款成功
func withdrawOrderSuccess(query string, order tblWithdraw) error {

	money := decimal.NewFromFloat(order.Amount)

	// 判断锁定余额是否充足
	_, err := lockBalanceIsEnough(order.UID, money)
	if err != nil {
		return err
	}

	myredis.AddUserFieldValueByDemical(order.UID, myConfig.G_tbl_member_balance+"lock_amount", money)
	return nil
}

func withdrawOrderFailed(query string, order tblWithdraw) error {

	money := decimal.NewFromFloat(order.Amount)
	moneyFee := decimal.NewFromFloat(order.Fee)

	//4、查询用户额度
	balance, ret := myUserHelp.GetMemberBalanceInfo(order.UID)
	if !ret {
		return errors.New(helper.UserNotExist)
	}

	blr := decimal.NewFromFloat(balance.Brl)
	balanceAfter := blr.Add(money)

	if meta.WalletMode == "1" {
		myredis.AddUserFieldValueByDemical(order.UID, myConfig.G_tbl_member_balance+"brl", money.Add(moneyFee))
		myredis.AddUserFieldValueByDemical(order.UID, myConfig.G_tbl_member_balance+"lock_amount", money.Add(moneyFee))
	} else if meta.WalletMode == "2" {
		myredis.AddUserFieldValueByDemical(order.UID, myConfig.G_tbl_member_balance+"brl", money.Add(moneyFee))
		myredis.AddUserFieldValueByDemical(order.UID, myConfig.G_tbl_member_balance+"lock_amount", money.Add(moneyFee))
		myredis.AddUserFieldValueByDemical(order.UID, myConfig.G_tbl_member_balance+"unlock_amount", money.Add(moneyFee))
	}

	//7、新增账变记录
	mbTrans := MemberTransaction{
		AfterAmount:  balanceAfter.String(),
		Amount:       money.String(),
		BeforeAmount: fmt.Sprintf(`%f`, balance.Brl),
		BillNo:       order.ID,
		CreatedAt:    time.Now().UnixMilli(),
		ID:           helper.GenId(),
		CashType:     helper.TransactionWithDrawFail,
		UID:          order.UID,
		Username:     order.Username,
	}

	query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(mbTrans).ToSQL()
	myredis.AddLogToRedis(query)

	//8、新增账变记录 手续费退回
	mbTransFee := MemberTransaction{
		AfterAmount:  balanceAfter.Add(moneyFee).String(),
		Amount:       moneyFee.String(),
		BeforeAmount: fmt.Sprintf(`%f`, balance.Brl),
		BillNo:       order.ID,
		CreatedAt:    time.Now().UnixMilli(),
		ID:           helper.GenId(),
		CashType:     helper.TransactionWithdrawalFeeRem,
		UID:          order.UID,
		Username:     order.Username,
	}

	query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(mbTransFee).ToSQL()
	myredis.AddLogToRedis(query)
	return nil
}

// 检查锁定钱包余额是否充足
func lockBalanceIsEnough(uid string, amount decimal.Decimal) (decimal.Decimal, error) {

	balance, ret := myUserHelp.GetMemberBalanceInfo(uid)
	if !ret {
		return decimal.NewFromFloat(balance.LockAmount), errors.New(helper.UserNotExist)
	}
	brl := decimal.NewFromFloat(balance.LockAmount)
	if brl.Sub(amount).IsNegative() {
		return brl, errors.New(helper.LackOfBalance)
	}

	return brl, nil
}

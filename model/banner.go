package model

import "errors"

func BannerList(d string) ([]byte, error) {

	var (
		data []byte
		err  error
	)

	pipe := meta.MerchantRedis.Pipeline()
	defer pipe.Close()

	exists := pipe.Exists(ctx, "banner")
	record := pipe.Get(ctx, "banner")
	pipe.Exec(ctx)

	if exists.Val() == 0 {
		return data, errors.New("notices not found")
	}

	data, err = record.Bytes()
	return data, err
}
